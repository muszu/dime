#pragma once

#include "constants.hpp"
#include "features.hpp"
#include "types.hpp"
#include "utils.hpp"
#include "dimensions.hpp"
#include "extent.hpp"

#include <tuple>
#include <vector>
#include <array>
#include <type_traits>
#include <memory>

namespace dime
{
enum DataType { UniqueData, SharedData, StaticData };

namespace detail
{
template<Extent... T, int I>
std::size_t calculate_size(std::tuple<T...> const & t, constant_<I>)
{
    if constexpr (I >= sizeof...(T))
    {
        return 1;
    }
    else
    {
        return std::get<I>(t).getSize() * calculate_size(t, constant<I+1>);
    }
}

std::size_t calculate_size(Features const & fs)
{
    if constexpr (std::tuple_size_v<decltype(fs(shape_predicate))> != 0)
    {
        static_assert(std::tuple_size_v<decltype(fs(shape_predicate))> == 1);
        return calculate_size(std::get<0>(fs(shape_predicate)).value, constant<0>);
    }
    else
    {
        return calculate_size(fs(dime::extent_predicate), constant<0>);
    }
}

template<VectorOrArray D, VectorOrArray S>
constexpr void copydata(D & destination, const S & source, const size_t & offset=0, const size_t & current_offset=1)
{
    if constexpr (VectorOrArray<typename S::value_type>)
    {
        auto new_offset = current_offset * source.size();
        for(int i=0; i<source.size(); i++)
        {
            copydata(destination, source.at(i), offset + i*current_offset, new_offset);
        }
    }
    else
    {
        for(int i=0; i<source.size(); i++)
        {
            destination.at(offset + i*current_offset) = source.at(i);
        }
    }
}

template<VectorOrArray D>
constexpr void perform_data_copying_if_needed(Features const & fs, D & destination)
{
    auto tuple_data = fs(data_predicate);
    if constexpr (std::tuple_size_v<decltype(tuple_data)> == 1)
    {
        detail::copydata(destination, std::get<0>(tuple_data).value);
    }
    else
    {
        static_assert(std::tuple_size_v<decltype(tuple_data)> == 0);
    }
}
}

template<DataType dt, typename T, SizeType ST>
struct data_facet
{
    static_assert(always_false_v<T>);// consider supporting references by using std::reference_wrapper
};

namespace detail
{
struct is_data_facet
{
    template <DataType dt, typename T, SizeType ST>
    constexpr auto operator()(data_facet<dt, T, ST>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
constexpr auto data_facet_predicate = [](Type t)
{
    return value(invoke_result(type<detail::is_data_facet>, t));
};
}

template<typename T>
struct data_facet<StaticData, T, static_size<0>>
{
    constexpr data_facet(Features const & fs)
    {
    }
};

template<typename T, std::size_t S>
struct data_facet<StaticData, T, static_size<S>>
{
    std::array<std::remove_cv_t<T>, S> source;

    constexpr data_facet(Features const & fs) : source(std::array<T, S>())
    {
        auto tuple_data_facet = fs(detail::data_facet_predicate);
        auto tuple_array_like = fs(detail::array_like_predicate);
        if constexpr (std::tuple_size_v<decltype(tuple_data_facet)> == 1)
        {
            if constexpr (static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<std::shared_ptr>, decltype(std::get<0>(tuple_data_facet).source)>{})
                || static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<std::unique_ptr>, decltype(std::get<0>(tuple_data_facet).source)>{}))
            {
                const auto & sourceData = std::get<0>(tuple_data_facet).source;
                for(int i=0; i<S; i++)
                {
                    source.at(i) = sourceData->at(i);
                }
            }
            else
            {
                const auto & sourceData = std::get<0>(tuple_data_facet).source;
                for(int i=0; i<S; i++)
                {
                    source.at(i) = sourceData.at(i);
                }
            }
        }
        else if constexpr(std::tuple_size_v<decltype(tuple_array_like)> == 1)
        {
            copy_loop<0>(source, std::get<0>(tuple_array_like), std::get<0>(tuple_array_like).outer(), 0, 1);
        }
        else
        {
            detail::perform_data_copying_if_needed(fs, source);
        }
    }

    const auto & access(std::size_t i) const
    {
        return source.at(i);
    }

    auto & access(std::size_t i) requires !std::is_const_v<T>
    {
        return source.at(i);
    }
};

template<typename T>
struct data_facet<UniqueData, T, dynamic_size>
{
    std::unique_ptr<std::vector<std::remove_cv_t<T>>> source;

    constexpr data_facet(Features const & fs)
    {
        auto tuple_data_facet = fs(detail::data_facet_predicate);
        if constexpr (std::tuple_size_v<decltype(tuple_data_facet)> == 1)
        {
            auto sourceData = std::get<0>(tuple_data_facet).source;

            if constexpr (static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<std::shared_ptr>, decltype(sourceData)>{})
                || static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<std::unique_ptr>, decltype(sourceData)>{}))
            {
                source = std::unique_ptr<std::vector<std::remove_cv_t<T>>>(new std::vector<std::remove_cv_t<T>>(*sourceData));
            }
            else
            {
                source = std::unique_ptr<std::vector<std::remove_cv_t<T>>>(new std::vector<std::remove_cv_t<T>>(sourceData.size()));
                for(int i=0; i<sourceData.size(); i++) 
                {
                    source->at(i) = sourceData.at(i);
                }
            }
        }
        else
        {
            auto tuple_array_like = fs(detail::array_like_predicate);
            source = std::unique_ptr<std::vector<std::remove_cv_t<T>>>(new std::vector<std::remove_cv_t<T>>(detail::calculate_size(fs)));
            if constexpr(std::tuple_size_v<decltype(tuple_array_like)> == 1)
            {
                copy_loop<0>(*source, std::get<0>(tuple_array_like), std::get<0>(tuple_array_like).outer(), 0, 1);
            }
            else
            {
                detail::perform_data_copying_if_needed(fs, *source);
            }
        }
        
    }
    constexpr data_facet(data_facet<UniqueData, T, dynamic_size> const & d) : source(std::unique_ptr<std::vector<std::remove_cv_t<T>>>(new std::vector<std::remove_cv_t<T>>(*d.source)))
    {
    }
    constexpr data_facet(data_facet<UniqueData, T, dynamic_size> & d) : source(std::unique_ptr<std::vector<std::remove_cv_t<T>>>(new std::vector<std::remove_cv_t<T>>(*d.source)))
    {
    }
    constexpr data_facet(data_facet<UniqueData, T, dynamic_size> && d) : source(std::unique_ptr<std::vector<std::remove_cv_t<T>>>(std::move(d.source)))
    {
    }

    const auto & access(std::size_t i) const
    {
        return (*source).at(i);
    }

    auto & access(std::size_t i) requires !std::is_const_v<T>
    {
        return (*source).at(i);
    }
};


template<typename T>
struct data_facet<SharedData, T, dynamic_size>
{
    std::shared_ptr<std::vector<std::remove_cv_t<T>>> source;

    constexpr data_facet(Features const & fs)
    {
        auto tuple_data_facet = fs(detail::data_facet_predicate);
        if constexpr (std::tuple_size_v<decltype(tuple_data_facet)> == 1)
        {
            if constexpr (static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<std::shared_ptr>, decltype(std::get<0>(tuple_data_facet).source)>{}))
            {
                source = std::shared_ptr<std::vector<std::remove_cv_t<T>>>(std::get<0>(tuple_data_facet).source);
            }
            else if constexpr (static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<std::unique_ptr>, decltype(std::get<0>(tuple_data_facet).source)>{}))
            {
                source = std::shared_ptr<std::vector<std::remove_cv_t<T>>>(new std::vector<std::remove_cv_t<T>>(*std::get<0>(tuple_data_facet).source));
            }
            else
            {
                auto sourceData = std::get<0>(tuple_data_facet).source;
                source = std::shared_ptr<std::vector<std::remove_cv_t<T>>>(new std::vector<std::remove_cv_t<T>>(sourceData.size()));
                for(int i=0; i<sourceData.size(); i++) 
                {
                    source->at(i) = sourceData.at(i);
                }
            }
        }
        else
        {
            auto tuple_array_like = fs(detail::array_like_predicate);
            source = std::shared_ptr<std::vector<std::remove_cv_t<T>>>(new std::vector<std::remove_cv_t<T>>(detail::calculate_size(fs)));
            if constexpr(std::tuple_size_v<decltype(tuple_array_like)> == 1)
            {
                copy_loop<0>(*source, std::get<0>(tuple_array_like), std::get<0>(tuple_array_like).outer(), 0, 1);
            }
            else
            {
                detail::perform_data_copying_if_needed(fs, *source);
            }
        }
    }
    constexpr data_facet(data_facet<SharedData, T, dynamic_size> const & d) : source(d.source)
    {
    }

    const auto & access(std::size_t i) const
    {
        return (*source).at(i);
    }

    auto & access(std::size_t i) requires !std::is_const_v<T>
    {
        return (*source).at(i);
    }
};

namespace detail
{
template<typename... T>
struct calculate_data_size {
    static constexpr std::size_t size = 0;
};
template<typename... T>
struct calculate_data_size<dim_facet<T...>> {
    static constexpr std::size_t size = calculate_data_size<T...>::size;
};
template<>
struct calculate_data_size<> {
    static constexpr std::size_t size = 1;
};
template<typename D, StaticOrArbitrarySize S, typename... T>
struct calculate_data_size<extent<D, S>, T...> {
    static constexpr std::size_t size = S::size * calculate_data_size<T...>::size;
};
}

template <DataType DT, ElementTypeFacet ETF, DimFacet DF>
using data_facet_from = data_facet<DT, typename ETF::type, typename std::conditional<DT == StaticData, static_size<detail::calculate_data_size<DF>::size>, dynamic_size>::type>;
}