#include "gtest/gtest.h"
#include "dime/constants.hpp"

namespace {

using namespace dime::ypo;

TEST(Constants, Arithmetic) {
    static_assert(constant<5> + constant<7> == constant<12>);
	static_assert(constant<constant<2>> == constant<2>);
	static_assert(15_c * -7_c == -105_c);
}

TEST(Constants, BaseConversion) {
    static_assert(0x12_c == 18_c);
	static_assert(-0x12_c == -022_c);
}

void accept_short(short)
{
	accept_short(50_c);
	// accept_short(500000_c); // overflow in conversion
}

}