#!/bin/bash

cmake -S . -B build -D CMAKE_BUILD_TYPE=Release \
    -D BUILD_TESTS=ON \
    -D QCK_TEST=OFF \
    -G Ninja \
&& cmake --build build \
&& ./build/test/run_all