#pragma once

#include "types.hpp"
#include "dimensions.hpp"
#include "extent.hpp"
#include "dim_facet.hpp"
#include "strides_facet.hpp"
#include "element_type_facet.hpp"
#include "data_facet.hpp"
#include "array_view.hpp"

#include <array>
#include <vector>
#include <tuple>
#include <type_traits>
#include <iostream>
#include <vector>

namespace dime
{

template<typename Shape, typename F, typename... T>
struct expression;
template<typename T>
struct index_converter;
template <typename... Facets>
struct dynamic_array;

struct dynamic_array_factory
{
    constexpr auto operator ()(DimensionType dimension)
    {
        return dimension;
    }

    constexpr auto operator ()(ElementType elementType)
    {
        return elementType;
    }

    constexpr auto operator ()(DeviceType deviceType)
    {
        return deviceType;
    }

    template<typename N, typename S>
    constexpr auto operator ()(extent<N, S> extent)
    {
        return extent;
    }

    template<Constant T, typename N>
    constexpr auto operator ()(assigned_dim<N, T> assigned_dim)
    {
        using Size = static_size<T::value>;
        return extent(std::remove_reference_t<N>(), Size{});
    }

    template<typename N>
    auto operator ()(assigned_dim<N, int> assigned_dim)
    {
        return extent(std::remove_reference_t<N>(), dynamic_size(assigned_dim.value));
    }

    template<VectorOrArray T>
    constexpr auto operator ()(T ds)
    {
        return std::make_tuple(
            data(ds),
            shape(getSTLConatinerShape(ds)),
            element_type<typename getSTLConatinerDataType<T>::type>);
    }

    template<typename... T>
    auto operator ()(std::tuple<T...> t)
    {
        static_assert((Extent<T> && ...));
        return t;
    }

    template<typename... T>
    auto operator ()(const expression<T...> & e)
    {
        return std::tuple_cat(std::forward_as_tuple(e, e.getElementType()), e.outer());
    }

    template<typename... T>
    auto operator ()(const array_view<T...> & e)
    {
        return std::tuple_cat(std::forward_as_tuple(e, element_type<typename array_view<T...>::ElementType>), e.outer());
    }

    template<typename... T>
    auto operator ()(const dynamic_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<const T &>(e)...);
    }
    template<typename... T>
    auto operator ()(dynamic_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<T &>(e)...);
    }
    template<typename... T>
    auto operator ()(dynamic_array<T...> && e)
    {
        return std::forward_as_tuple(static_cast<T &&>(e)...);
    }

    template<typename... T>
    auto operator ()(const shared_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<const T &>(e)...);
    }
    template<typename... T>
    auto operator ()(shared_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<T &>(e)...);
    }
    template<typename... T>
    auto operator ()(shared_array<T...> && e)
    {
        return std::forward_as_tuple(static_cast<T &&>(e)...);
    }
    
    template<typename... T>
    auto operator ()(const static_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<const T &>(e)...);
    }
    template<typename... T>
    auto operator ()(static_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<T &>(e)...);
    }
    template<typename... T>
    auto operator ()(static_array<T...> && e)
    {
        return std::forward_as_tuple(static_cast<T &&>(e)...);
    }
};

template <typename... Facets>
struct dynamic_array : public form<dynamic_array_factory, Facets...>
{
    using form<dynamic_array_factory, Facets...>::form;
    template<typename... T>
    constexpr dynamic_array(const expression<T...> & e) : form<dynamic_array_factory, Facets...>(e)
    {
    };
    template<typename... T>
    constexpr dynamic_array(const array_view<T...> & e) : form<dynamic_array_factory, Facets...>(e)
    {
    };
    constexpr auto operator() ()
    {
        return array_view(*this);
    }
    template<typename... T>
        requires std::tuple_size_v<typename std::tuple_element_t<0, std::tuple<Facets...>>::Outer> == sizeof...(T)
            && !(std::tuple_size_v<typename index_converter<T>::type::Outer> || ...)
    constexpr auto & operator() (const T & ... t)
    {
        return this->at(t...);
    }
    template<typename... T>
        requires !(std::tuple_size_v<typename std::tuple_element_t<0, std::tuple<Facets...>>::Outer> == sizeof...(T)
            && !(std::tuple_size_v<typename index_converter<T>::type::Outer> || ...))
    constexpr auto operator() (const T & ... t)
    {
        return array_view(*this, t...);
    }

    template<typename... Ts>
    const auto & at(std::tuple<Ts...> const & t) const {
        return this->access(this->getTotalOffsetTuple(t));
    }
    template<typename... Ts>
    constexpr auto & at(std::tuple<Ts...> const & t) {
        return this->access(this->getTotalOffsetTuple(t));
    }
    template<typename... Ts>
    const auto & at(const Ts & ... ts) const {
        return this->access(this->getTotalOffset(ts...));
    }    
    template<typename... Ts>
    constexpr auto & at(const Ts & ... ts) {
        return this->access(this->getTotalOffset(ts...));
    }
};

template <typename... Facets>
struct shared_array : public form<dynamic_array_factory, Facets...>
{
    using form<dynamic_array_factory, Facets...>::form;
    template<typename... T>
    constexpr shared_array(const expression<T...> & e) : form<dynamic_array_factory, Facets...>(e)
    {
    };
    template<typename... T>
    constexpr shared_array(const array_view<T...> & e) : form<dynamic_array_factory, Facets...>(e)
    {
    };
   template<typename... T>
        requires std::tuple_size_v<typename std::tuple_element_t<0, std::tuple<Facets...>>::Outer> == sizeof...(T)
            && !(std::tuple_size_v<typename index_converter<T>::type::Outer> || ...)
    constexpr auto & operator() (const T & ... t)
    {
        return this->at(t...);
    }
    template<typename... T>
        requires !(std::tuple_size_v<typename std::tuple_element_t<0, std::tuple<Facets...>>::Outer> == sizeof...(T)
            && !(std::tuple_size_v<typename index_converter<T>::type::Outer> || ...))
    constexpr auto operator() (const T & ... t)
    {
        return array_view(*this, t...);
    }

    template<typename... Ts>
    const auto & at(std::tuple<Ts...> const & t) const {
        return this->access(this->getTotalOffsetTuple(t));
    }
    template<typename... Ts>
    constexpr auto & at(std::tuple<Ts...> const & t) {
        return this->access(this->getTotalOffsetTuple(t));
    }
    template<typename... Ts>
    const auto & at(const Ts & ... ts) const {
        return this->access(this->getTotalOffset(ts...));
    }    
    template<typename... Ts>
    constexpr auto & at(const Ts & ... ts) {
        return this->access(this->getTotalOffset(ts...));
    }
};

template <typename... Initializers>
dynamic_array(Initializers...) -> dynamic_array<
    dim_facet_from<features_from<dynamic_array_factory, Initializers...>>,
    strides_facet_from<
        dim_facet_from<features_from<dynamic_array_factory, Initializers...>>,
        features_from<dynamic_array_factory, Initializers...>>,
    element_type_facet_from<features_from<dynamic_array_factory, Initializers...>>,
    data_facet_from<
        UniqueData,
        element_type_facet_from<features_from<dynamic_array_factory, Initializers...>>,
        dim_facet_from<features_from<dynamic_array_factory, Initializers...>>>
>;
template<typename... T>
dynamic_array(const expression<T...> &) -> dynamic_array<
    dim_facet_from<features_from<dynamic_array_factory, expression<T...>>>,
    strides_facet_from<
        dim_facet_from<features_from<dynamic_array_factory, expression<T...>>>,
        features_from<dynamic_array_factory, expression<T...>>>,
    element_type_facet_from<features_from<dynamic_array_factory, expression<T...>>>,
    data_facet_from<
        UniqueData,
        element_type_facet_from<features_from<dynamic_array_factory, expression<T...>>>,
        dim_facet_from<features_from<dynamic_array_factory, expression<T...>>>>
>;
template<typename... T>
dynamic_array(const array_view<T...> &) -> dynamic_array<
    dim_facet_from<features_from<dynamic_array_factory, array_view<T...>>>,
    strides_facet_from<
        dim_facet_from<features_from<dynamic_array_factory, array_view<T...>>>,
        features_from<dynamic_array_factory, array_view<T...>>>,
    element_type_facet_from<features_from<dynamic_array_factory, array_view<T...>>>,
    data_facet_from<
        UniqueData,
        element_type_facet_from<features_from<dynamic_array_factory, array_view<T...>>>,
        dim_facet_from<features_from<dynamic_array_factory, array_view<T...>>>>
>;

template <typename... Initializers>
shared_array(Initializers...) -> shared_array<
    dim_facet_from<features_from<dynamic_array_factory, Initializers...>>,
    strides_facet_from<
        dim_facet_from<features_from<dynamic_array_factory, Initializers...>>,
        features_from<dynamic_array_factory, Initializers...>>,
    element_type_facet_from<features_from<dynamic_array_factory, Initializers...>>,
    data_facet_from<
        SharedData,
        element_type_facet_from<features_from<dynamic_array_factory, Initializers...>>,
        dim_facet_from<features_from<dynamic_array_factory, Initializers...>>>
>;
template<typename... T>
shared_array(const expression<T...> &) -> shared_array<
    dim_facet_from<features_from<dynamic_array_factory, expression<T...>>>,
    strides_facet_from<
        dim_facet_from<features_from<dynamic_array_factory, expression<T...>>>,
        features_from<dynamic_array_factory, expression<T...>>>,
    element_type_facet_from<features_from<dynamic_array_factory, expression<T...>>>,
    data_facet_from<
        SharedData,
        element_type_facet_from<features_from<dynamic_array_factory, expression<T...>>>,
        dim_facet_from<features_from<dynamic_array_factory, expression<T...>>>>
>;
template<typename... T>
shared_array(const array_view<T...> &) -> shared_array<
    dim_facet_from<features_from<dynamic_array_factory, array_view<T...>>>,
    strides_facet_from<
        dim_facet_from<features_from<dynamic_array_factory, array_view<T...>>>,
        features_from<dynamic_array_factory, array_view<T...>>>,
    element_type_facet_from<features_from<dynamic_array_factory, array_view<T...>>>,
    data_facet_from<
        UniqueData,
        element_type_facet_from<features_from<dynamic_array_factory, array_view<T...>>>,
        dim_facet_from<features_from<dynamic_array_factory, array_view<T...>>>>
>;

template <typename T, auto... Initializers>
concept DynamicArrayForm = Form<T, dynamic_array_factory, Initializers...>;

}
