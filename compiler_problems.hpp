namespace comp_1
{
template <typename>
concept DimensionName = true;

template<DimensionName... Ns>
struct dimension_list {
    constexpr dimension_list(){};
    constexpr dimension_list(const dimension_list<Ns...> &){};

    // using concept in this template causes compiler error
    template<DimensionName... Ts>
    constexpr auto operator,(const dimension_list<Ts...> &)
    {
        return dimension_list<Ns..., Ts...>();
    }
};

template <DimensionName N = decltype([]{})>
struct dimension : dimension_list<N> {
    constexpr dimension() {};
    constexpr dimension(const dimension<N> &) {};
};

void fail() {
    dimension d;
}
}