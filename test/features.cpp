#include "gtest/gtest.h"
#include "dime/constants.hpp"
#include "dime/features.hpp"
#include "dime/types.hpp"

#include <array>
#include <cmath>
#include <tuple>

namespace {

using namespace dime::ypo;

// Just a forward declaration...
template <std::size_t N>
struct int_facet;


// # Concepts
//
// This small infrastructural library helps to build convenient to use, flexible user-facing types.
// It is based on three simple concepts:
//
// - feature: a piece of information influencing the types being built;
// - facet: a single aspect of functionality;
//   may be shared among multiple user-facing types;
//   defines a mapping from sequences of features to types implementing the facet;
// - form: user-facing class (template);
//   combines (inherits) one or more facets;
//   defines a many-to-many mapping from initializers (constructor arguments) to features.


// # Usage
//
// ## Define your features.
// Use any types you want, they only need to be movable.

struct int_feature
{
    int value;

    constexpr int_feature(int value) :
        value(value)
    {
    }

    constexpr int_feature(int_feature &&) = default; // moving allowed, copying not

    // If you want the feature to be *verifiable* (used in form concepts),
    // implement operator(), taking a type and returning whether it matches:
    template <std::size_t N>
    constexpr auto check(int_facet<N> const &) const
    {
        return constant<N>;
    }

    constexpr auto check(...) const
    {
        return constant<0>;
    }

    constexpr bool operator ()(Type t) const
    {
        using n = decltype(check(std::declval<typename decltype(t)::type const &>()));
        return int(n::value) >= value;
    }
};

struct float_feature
{
};


// ## Implement the facets.
// Usually, a facet is implemented as a family of classes (often a single class template).
// All classes in such family need to be constructible from a Features object.
// To filter features of relevant types, you can define a predicate on types:

constexpr auto int_feature_predicate = [](Type t)
{
    return t == type<int_feature>;
};

template <std::size_t N>
struct int_facet : std::array<int, N>
{
    constexpr int_facet(Features const & fs) :
        std::array<int, N>(std::apply([](auto && ... fs){ return std::array { fs.value... }; }, fs(int_feature_predicate)))
    {
    }
};

struct float_facet
{
    constexpr float_facet(Features const &)
    {
    }
};

// The facet is defined by an alias template,
// taking the exact type of the above constructor argument, and returning the type to be constructed:

template <Features F>
using int_facet_from = int_facet<std::tuple_size_v<std::invoke_result_t<F, decltype(int_feature_predicate)>>>;

template <Features F>
using float_facet_from = float_facet;

// Facets can depend on (inherit) other facets.
// In such case, we recommend the exact type(s) implementing the base facet(s)
// to become a template argument of the derived facet.
//
// TODO: Currently, inheriting a single facet multiple times (diamond inheritance)
// will create data duplication (and inhibit empty base class optimization).
// CRTP can be used to solve this problem, but it makes the names of the resulting types
// overly long and more difficult to understand.


// ## Compose facets into forms.
//
// Each form needs to provide a feature factory -- a constexpr-default-constructible type,
// whose operator() transforms form initializers to features.
// If more (or less) than one feature is to be created, use std::tuple.

struct near_factory
{
    constexpr auto operator ()(int value)
    {
        return int_feature(value);
    }

    constexpr auto operator ()(float value)
    {
        return std::tuple { float_feature(), int_feature(std::floor(value)), int_feature(std::ceil(value)) };
    }
};

// Each form should look as the following class template
// (any data/behavior is better placed in the facets).

template <typename... Facets>
struct near_form : form<near_factory, Facets...>
{
    using form<near_factory, Facets...>::form;
};

// To enable the form construction machinery, provide a single (universal) deduction guide --
// it should map the types of initializers through the features type into the facet types:

template <typename... Initializers>
near_form(Initializers...) -> near_form<
    int_facet_from<features_from<near_factory, Initializers...>>,
    float_facet_from<features_from<near_factory, Initializers...>>>;


// You can also create a form concept.
// Simply specify how its initializers map into (verifiable) features
// (you may use the same factory as above, or a different one):

template <typename T, auto... Initializers>
concept NearForm = Form<T, near_factory, Initializers...>;


// And here is how it works:

constexpr bool at_least_two(NearForm<2> const &)
{
    return true;
}

constexpr bool at_least_two(auto const &)
{
    return false;
}

TEST(Features, Form) {
    constexpr near_form f1(5);
    static_assert(sizeof(f1) == 1*sizeof(int));
    static_assert(f1[0] == 5);
    static_assert(not at_least_two(f1));

    constexpr near_form f2(2, 5.3f);
    static_assert(sizeof(f2) == 3*sizeof(int));
    static_assert(f2[0] == 2);
    static_assert(f2[1] == 5);
    static_assert(f2[2] == 6);
    static_assert(at_least_two(f2));
}

}
