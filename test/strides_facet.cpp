#include "gtest/gtest.h"
#include "dime/static_array.hpp"
#include "dime/extent.hpp"
#include "dime/types.hpp"

#include <iostream>
#include <array>

using namespace dime;

namespace {

TEST(StridesFacet, GetOffset) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    
    constexpr static_array a(x=2_c);
    EXPECT_EQ(a.getOffset(x), 1);
    EXPECT_EQ(a.getTotalOffset(x=0, y=5, z=42), 0);
    
    constexpr static_array b(x=42_c, y=3_c, z=5_c);
    EXPECT_EQ(b.getOffset(x), 1);
    EXPECT_EQ(b.getOffset(y), 42);
    EXPECT_EQ(b.getOffset(z), 42*3);
    EXPECT_EQ(b.getTotalOffset(x=13, y=1, z=0), 13+42);
    
    constexpr static_array c(~x, y=3_c);
    EXPECT_EQ(c.getOffset(x), 0);
    EXPECT_EQ(c.getOffset(y), 1);
    EXPECT_EQ(c.getTotalOffset(x=50, y=2), 2);

    constexpr static_array d(y=3_c, ~x);
    EXPECT_EQ(d.getOffset(x), 0);
    EXPECT_EQ(d.getOffset(y), 1);
    EXPECT_EQ(d.getTotalOffset(x=42, y=2, z=3), 2);

    EXPECT_TRUE(
        (Constant<decltype(d.getOffset(y))>));

    EXPECT_TRUE(
        (Constant<decltype(d.getTotalOffset(x=42, y=2_c, z=3))>));

    constexpr static_array e(y=3_c, ~x, z=5_c);
    EXPECT_EQ(e.getOffset(y), 1);
    EXPECT_EQ(e.getOffset(x), 0);
    EXPECT_EQ(e.getOffset(z), 3);
    EXPECT_EQ(e.getTotalOffset(x=42, y=2, z=3), 2+9);
}

}