#pragma once

#include "constants.hpp"
#include "features.hpp"
#include "types.hpp"
#include "utils.hpp"
#include "extent.hpp"
#include "dim_facet.hpp"

#include <tuple>
#include <type_traits>

using namespace dime::ypo;

namespace dime
{

template<typename N>
struct stride_base
{
    using dimension_type = N;
};

template<typename N, std::size_t S>
struct static_stride : public stride_base<N> {
    constexpr auto static size()
    {
        return constant<S>;
    }
};

template<typename N>
struct dynamic_stride : public stride_base<N> {
    std::size_t _size = 0;

    dynamic_stride() : _size(0) {}
    dynamic_stride(std::size_t _size) : _size(_size) {}
    dynamic_stride(const dynamic_stride<N> &ds) : _size(ds.size()) {}

    std::size_t size() const
    {
        return _size;
    }

    protected:
    void setSize(std::size_t s)
    {
        _size = s;
    }
};

namespace detail
{
struct is_static_stride
{
    template <typename T, std::size_t S>
    constexpr auto operator()(static_stride<T, S>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
struct is_dynamic_stride
{
    template <typename T>
    constexpr auto operator()(dynamic_stride<T>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
}

template<typename T>
concept StaticStride = static_cast<bool>(std::invoke_result_t<detail::is_static_stride, T>{});
template<typename T>
concept DynamicStride = static_cast<bool>(std::invoke_result_t<detail::is_dynamic_stride, T>{});
template<typename T>
concept Stride = StaticStride<T> || DynamicStride<T>;

namespace detail
{
template<typename... T>
struct contains_dynamic_stride : std::false_type {};
template<>
struct contains_dynamic_stride<> : std::false_type {};
template<DynamicStride N, typename... T>
struct contains_dynamic_stride<N, T...> : std::true_type {};
template<typename D, typename... T>
struct contains_dynamic_stride<D, T...> : contains_dynamic_stride<T...> {};

template<typename... T, int AnonymousDimsCount, int Check>
constexpr auto get_anonymous_extend_position_aux(const std::tuple<T...> &t, const constant_<AnonymousDimsCount> &c, const constant_<Check> &)
{
    if constexpr (Check < sizeof...(T))
    {
        if constexpr (static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<anonymous_dimension>, decltype(std::get<Check>(t).dimension)>{}))
        {
            if constexpr (AnonymousDimsCount == 0)
            {
                return constant<Check>;
            }
            else
            {
                return get_anonymous_extend_position_aux(t, constant<AnonymousDimsCount-1>, constant<Check+1>);
            }
            
        }
        else
        {
            return get_anonymous_extend_position_aux(t, c, constant<Check+1>);
        }
    }
    else
    {
        return constant<-1>;
    }
}
}

template<typename... T, int AnonymousDimsCount>
constexpr auto get_anonymous_extend_position(const std::tuple<T...> &t, const constant_<AnonymousDimsCount> &c)
{
    return detail::get_anonymous_extend_position_aux(t, c, constant<0>);
}

template<typename... T>
struct find_matching_extent
{
    static_assert(always_false_v<T...>);
};
template<typename D, typename T, typename... Ts>
struct find_matching_extent<D, T, Ts...>
{
    using type = std::conditional_t<std::is_same_v<typename T::dimension_type, D>, T, typename find_matching_extent<D, Ts...>::type>;
};
template<typename D>
struct find_matching_extent<D>
{
    using type = bool;
};

template<typename T, typename... Ts>
struct contains_equal
{
    static const bool value = (... || std::is_same_v<T, Ts>);
};
template<typename... T>
struct contains_equal_in
{
    static_assert(always_false_v<T...>);
};
template<typename T, typename... Ts>
struct contains_equal_in<T, std::tuple<Ts...>>
{
    static const bool value = contains_equal<T, Ts...>::value;
};
template<typename... T>
struct contains_all
{
    static_assert(always_false_v<T...>);
};
template<typename... V, typename T, typename... Ts>
struct contains_all<std::tuple<V...>, T, Ts...>
{
    static constexpr bool value = contains_equal<T, V...>::value && contains_all<std::tuple<V...>, Ts...>::value;
};
template<typename... V>
struct contains_all<std::tuple<V...>>
{
    static constexpr bool value = true;
};

template <Stride... T>
    requires sizeof...(T) != 0
struct strides_facet : T...
{
    constexpr strides_facet(Features const & fs)
    {
        // Run-time initialization is only needed when strides facet contains a dynamic stride.
        if constexpr (detail::contains_dynamic_stride<T...>::value)
        {
            initialize_dynamic_strides<T...>(fs, 1, constant<0>);
        }
    }
    constexpr strides_facet(strides_facet<T...> const & s) : T(static_cast<T const &>(s))...
    {
    }

    // Shape can contain extend with anonymous dimension that was named.
    // AnonymousDimsCount counter keep track which anonymous dimension should be used to obtain size.
    template<typename U, typename... Us, int AnonymousDimsCount> // requires U, Us strides
    void initialize_dynamic_strides(Features const & fs, std::size_t s, constant_<AnonymousDimsCount> c)
    {
        if constexpr (DynamicStride<U>)
        {
            U::setSize(s);
        }
        // 1. try finding extend with dimension = U::dimension
        auto ext = fs(extend_with_name_predicate<typename U::dimension_type>);
        static_assert(std::tuple_size_v<decltype(ext)> <= 1, "This tuple should not have more then one element.");
        if constexpr (std::tuple_size_v<decltype(ext)> == 1)
        {
            std::size_t extend_size = std::get<0>(ext).size.size;
            if constexpr (sizeof...(Us) != 0)
            {
                initialize_dynamic_strides<Us...>(fs, s * extend_size, c);
            }
        }
        // 2. if failed then find anonymous dimension number AnonymousDimsCount (in shape) and increment counter
        else
        {
            auto tpl_sh = fs(shape_predicate);
            static_assert(std::tuple_size_v<decltype(tpl_sh)> == 1);
            auto sh = std::get<0>(tpl_sh).value;
            constexpr int index = get_anonymous_extend_position(sh, c).value;
            static_assert(index != -1, "Matching anonymous dimension wasn't found");

            std::size_t extend_size = std::get<index>(sh).size.size;

            if constexpr (sizeof...(Us) != 0)
            {
                initialize_dynamic_strides<Us...>(fs, s * extend_size, constant<AnonymousDimsCount + 1>);
            }
        }
    }

    constexpr std::array<std::size_t, sizeof...(T)> strides()
    {
        return {T::size()... };
    }

    std::string stridesString() const
    {
        std::string result = "Strides:\n";
        ((result += (std::string(ypo::type_name<T>()) + " = " + std::to_string(T::size()) + ",\n")), ...);
        result.erase(result.end() - 2);
        return result;
    }

    template<DimensionType D>
    constexpr auto getOffset(const D &) const
    {
        using StrideType = typename find_matching_extent<D/*::dimension_name*/, T...>::type;
        return StrideType::size();
    }

    template<DimensionType D>
    constexpr auto getOffset() const
    {
        using StrideType = typename find_matching_extent<D/*::dimension_name*/, T...>::type;
        return StrideType::size();
    }

    template<size_t I, typename... Ts>
    constexpr auto _getTotalOffset(const std::tuple<Ts...>& t) const {
        if constexpr(I == sizeof...(Ts)) {
            return constant<0>;
        }
        else
        {
            using CurrentType = std::tuple_element_t<I, std::tuple<std::remove_cv_t<std::remove_reference_t<Ts>>...>>;
            using CurrentDimension = typename CurrentType::dimension_type;

            if constexpr (contains_equal<CurrentDimension, typename T::dimension_type...>::value)
            {
                if constexpr (std::is_same_v<static_stride<CurrentDimension, 0>, typename find_matching_extent<CurrentDimension, T...>::type>)
                {
                    return _getTotalOffset<I + 1>(t);
                }
                else
                {
                    return (std::get<I>(t).value * getOffset<CurrentDimension>()) + _getTotalOffset<I + 1>(t);
                }
            }
            else
            {
                return _getTotalOffset<I + 1>(t);
            }
        }
    }

    template<typename... Ts>
    constexpr auto getTotalOffsetTuple(std::tuple<Ts...> const & t) const {
        static_assert(contains_all<std::tuple<typename std::remove_cv_t<std::remove_reference_t<Ts>>::dimension_type...>, typename T::dimension_type...>::value);
        return _getTotalOffset<0>(t);
    }
    template<typename... Ts>
    constexpr auto getTotalOffset(const Ts & ... ts) const {
        return getTotalOffsetTuple(std::forward_as_tuple(ts...));
    }
};

template<typename T>
concept StridesFacet = static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<strides_facet>, T>{});

struct dynamic_stride_from_extent
{
    template<typename Name, typename Size>
    constexpr auto operator()(const extent<Name, Size> &)
    {
        return dynamic_stride<Name>();
    }
    template<typename Name>
    constexpr auto operator()(const extent<Name, arbitrary_size> &)
    {
        return static_stride<Name, 0>();
    }
};

template<std::size_t S, typename... T>
struct create_strides_from_extents {
    static_assert(always_false_v<T...>);
};
template<std::size_t S>
struct create_strides_from_extents<S> {
    using type = std::tuple<>;
};
template<std::size_t S, typename N, Extent... E>
struct create_strides_from_extents<S, extent<N, dynamic_size>, E...> {
    using type = std::tuple<dynamic_stride<N>, std::invoke_result_t<dynamic_stride_from_extent, E>...>;
};
template<std::size_t S, typename N, Extent... E>
struct create_strides_from_extents<S, extent<N, arbitrary_size>, E...> {
    using rec = typename create_strides_from_extents<S, E...>::type;
    
    using type = typename detail::tuple_cat_type<
            std::tuple<static_stride<N, 0>>,
            rec
        >::type;
};
template<std::size_t S, typename N, typename Size, Extent... E>
struct create_strides_from_extents<S, extent<N, Size>, E...> {
    static constexpr std::size_t extentSize = Size::size;
    
    using rec = typename create_strides_from_extents<extentSize == 0 ? S : extentSize * S, E...>::type;
    
    using type = typename detail::tuple_cat_type<
            std::tuple<static_stride<N, extentSize == 0 ? 0 : S>>,
            rec
        >::type;
};

template<typename... T>
struct create_strides_from_dim_facet {
    static_assert(always_false_v<T...>);
};
template<Extent... E>
struct create_strides_from_dim_facet<dim_facet<E...>> {
    using type = typename create_strides_from_extents<1, E...>::type;
};
template<typename... T>
struct strides_features_to_strides_facet
{
    static_assert(always_false_v<T...>);
};
template<DimFacet DF, StridesFacet T>
struct strides_features_to_strides_facet<DF, std::tuple<T>>
{
    using type = std::remove_cv_t<std::remove_reference_t<T>>;
};
template<DimFacet DF>
struct strides_features_to_strides_facet<DF, std::tuple<>>
{
    // For static array, strides can be calculated from dim_facet alone.
    using strides_tuple = typename create_strides_from_dim_facet<DF>::type;
    using type = typename detail::template_type_from_tuple<strides_facet, strides_tuple>::type;
};

constexpr auto strides_feature_predicate = [](Type t)
{
    return value(invoke_result(type<detail::is_instance_of_template<strides_facet>>, t));
};

template <DimFacet DF, Features F>
using strides_facet_from = typename strides_features_to_strides_facet<DF, std::invoke_result_t<F, decltype(strides_feature_predicate)>>::type;

}
