#include "gtest/gtest.h"
#include "dime/dimensions.hpp"
#include <array>
#include <vector>
#include <iostream>

namespace {

using namespace dime;

namespace test_using_function_arg_in_template_ns {
    dimension<struct x> x;
    dimension d;

    template <DimensionType D>
    struct teststruct {};
    void test_one(DimensionType d1)
    {
        teststruct<decltype(d1)> ts;
    }
    template <DimensionType D1, DimensionType D2>
    void test_two(D1 d1, D2 d2)
    {
        teststruct<D1> td1;
        teststruct<D2> td2;
    }
}

TEST(Dimensions, TestUsingDimensionInTemplate) {
    dimension qq, dd;
    test_using_function_arg_in_template_ns::test_one(qq);
    test_using_function_arg_in_template_ns::test_two(qq, dd);
}

TEST(Dimension_list, Copy_constructor) {
    dimension y,z;
    dimension<struct x> x;
    using yName = decltype(y)::dimension_name;
    using zName = decltype(z)::dimension_name;
    dimension_list<struct x, yName, zName> qwer = (x,y,z);
}

TEST(Dimensions, Assignment) {
    dimension x,z;
    dimension<struct y> y;
    std::vector<int> v = {7, 5, 16, 8};
    std::array<int, 3> a = {1, 2, 3};
    x=4;
    (x,z,y) = 4;
    x=v;
    (x,z,y) = {{1,2,3}, {1,2,3}};
    x=a;
    x={1,2,3};
    (x,y)={ {1, 11}, {2, 22}, {3, 33} };
}

TEST(Dimensions, CombineSingle) {
    extent<dimension<int>, dynamic_size> d1(1), d2(2);
    extent<dimension<int>, static_size<1>> s1;
    extent<dimension<int>, static_size<2>> s2;
    extent<dimension<int>, arbitrary_size> a;
    extent<dimension<int>, blank_size> b;

    auto cs1 = dime::detail::combine_single(d1, s1);
    EXPECT_TRUE(
        (std::is_same_v<decltype(cs1), decltype(s1)>));
    auto cs2 = dime::detail::combine_single(s2, s2);
    EXPECT_TRUE(
        (std::is_same_v<decltype(cs2), decltype(s2)>));
    auto cs3 = dime::detail::combine_single(a, b);
    EXPECT_TRUE(
        (std::is_same_v<decltype(cs3), decltype(a)>));
    
    try
    {
        dime::detail::combine_single(d1, s2);
        FAIL() << "Expected dime::incompatible_sizes_exception";
    }
    catch(incompatible_sizes_exception const & err)
    {
        EXPECT_EQ(err.what(), std::string("Combine called with incompatible sizes."));
    }
    catch(...)
    {
        FAIL() << "Expected dime::incompatible_sizes_exception";
    }
}

TEST(Dimensions, Combine) {
    extent<dimension<int>, dynamic_size> d1(1), d2(2), d3(3);
    extent<dimension<int>, static_size<1>> s1;
    extent<dimension<int>, static_size<2>> s2;
    extent<dimension<int>, static_size<3>> s3;
    extent<dimension<int>, arbitrary_size> a;
    extent<dimension<int>, blank_size> b;

    auto t = std::make_tuple(d2, s2);

    try
    {
        combineExtents(a, s2, d3);
        FAIL() << "Expected dime::incompatible_sizes_exception";
    }
    catch(incompatible_sizes_exception const & err)
    {
        EXPECT_EQ(err.what(), std::string("Combine called with incompatible sizes."));
    }
    catch(...)
    {
        FAIL() << "Expected dime::incompatible_sizes_exception";
    }
}

}