#pragma once

#include "constants.hpp"

#include <type_traits>

namespace dime::ypo {

namespace detail
{
	template <typename, template <typename...> typename>
	struct is_instance : std::false_type {};

	template <typename... Ts, template <typename...> typename T>
	struct is_instance<T<Ts...>, T> : std::true_type {};
}

template <class T>
constexpr std::string_view type_name() {
	std::string_view p = __PRETTY_FUNCTION__;
	return std::string_view(p.data() + 60, p.size() - 60 - 49 - 1);
}

template <class T>
constexpr std::string_view type_name(const T&) {
	return type_name<T>();
}

template <typename T>
struct type_
{
	constexpr static std::string_view type_name()
	{
		return dime::ypo::type_name<T>();
	}

	using type = T;

	constexpr type_(const auto &...)
	{
	}

	constexpr explicit operator bool() const
	{
		return static_cast<bool>(T{});
	}

	friend constexpr auto operator ==(type_<T>, type_<T>)
	{
		return constant<true>;
	}

	friend constexpr auto operator ==(type_<T>, type_<auto>)
	{
		return constant<false>;
	}

	friend constexpr auto operator !=(type_<T> t1, type_<auto> t2)
	{
		return not (t1 == t2);
	}

	template <typename T2>
	friend constexpr auto conditional(Constant<bool> c, type_<T>, type_<T2>)
	{
		return type_<std::conditional_t<c.value, T, T2>>{};
	}

	template <bool C>
	friend constexpr auto conditional(type_<T> true_type, type_<auto> false_type)
	{
		return conditional(constant<C>, true_type, false_type);
	}

	template <typename T2>
	friend constexpr auto is_base_of(type_<T> base, type_<T2> derived)
	{
		return constant<std::is_base_of_v<T, T2>>;
	}

	template <template <typename...> typename T2>
	friend constexpr auto is_instance_of_template(type_<T>)
	{
		return constant<detail::is_instance<T, T2>::value>;
	}

	friend constexpr T value(type_<T>)
	{
		return {};
	}

	template <typename... Ts>
	friend constexpr auto invoke_result(type_<T>, type_<Ts>...)
	{
		return type_<std::invoke_result_t<T, Ts...>>{};
	}
};

template <typename T>
type_(T &&) -> type_<std::remove_reference_t<T>>;


template <typename T>
constexpr auto type = type_<T>{};


namespace detail
{
	struct is_type
	{
		constexpr auto operator() (type_<auto>) const
		{
			return constant<true>;
		}

		constexpr auto operator() (...) const
		{
			return constant<false>;
		}
	};
}

template <typename T>
concept Type = static_cast<bool>(std::invoke_result_t<detail::is_type, T>{});


template <auto T>
requires Type<decltype(T)>
using type_type = typename decltype(T)::type;


template <template <typename...> typename TT, typename... Ts>
constexpr auto guide(type_<Ts>... ts)
{
	return type<decltype(TT(value(ts)...))>;
}
}
