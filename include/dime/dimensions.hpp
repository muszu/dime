#pragma once

#include "constants.hpp"
#include "utils.hpp"

#include <type_traits>
#include <utility>
#include <array>
#include <iostream>
#include <tuple>

using namespace dime::ypo;

namespace dime {

template <typename D, typename Value>
struct assigned_dim
{
    using value_type = Value;
    using dimension_type = D;
    constexpr static D dimension {};
    const Value & value;

    constexpr assigned_dim(const Value & value) : value(value) {}
};
template <typename D, auto S>
struct assigned_dim<D, constant_<S>>
{
    using value_type = constant_<S>;
    using dimension_type = D;
    constexpr static D dimension {};
    constexpr static constant_<S> value {};

    constexpr assigned_dim(const constant_<S> &) {}
};
template <typename D>
struct assigned_dim<D, int>
{
    using value_type = int;
    using dimension_type = D;
    constexpr static D dimension {};
    const int value;

    constexpr assigned_dim(const int & value) : value(value) {}
};
template <typename D>
struct assigned_dim<D, size_t>
{
    using value_type = size_t;
    using dimension_type = D;
    constexpr static D dimension {};
    const size_t value;

    constexpr assigned_dim(const size_t & value) : value(value) {}
};
template <typename D, Dimension D2>
struct assigned_dim<D, D2>
{
    using value_type = D2;
    using dimension_type = D;
    constexpr static D dimension {};
    D2 value = D2();
};
template <typename D, typename T, size_t S>
struct assigned_dim<D, std::array<T, S>>
{
    using value_type = std::array<T, S>;
    using dimension_type = D;
    constexpr static D dimension {};
    const std::array<T, S> value;

    constexpr assigned_dim(const std::array<T, S> & value) : value(value) {}
    constexpr assigned_dim(std::array<T, S> & value) : value(value) {}
    constexpr assigned_dim(std::array<T, S> && value) : value(std::move(value)) {}
};

template <std::size_t S>
struct static_size {
    static constexpr size_t size = S;
};

struct arbitrary_size {
    static constexpr std::size_t size = 1;
};

struct dynamic_size {
    std::size_t size;

    constexpr dynamic_size() : size(0) {};
    constexpr dynamic_size(std::size_t s) : size(s) {};
    constexpr dynamic_size(const dynamic_size & d) : size(d.size) {};
};

// Size used in indexing, when replacing existing dimension it inherits its size.
struct blank_size {
    static constexpr size_t size = 0;
};

namespace detail
{
struct is_static_size
{
    template <std::size_t S>
    constexpr auto operator()(static_size<S>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
struct is_arbitrary_size
{
    constexpr auto operator()(arbitrary_size)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
struct is_dynamic_size
{
    constexpr auto operator()(dynamic_size)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
struct is_blank_size
{
    constexpr auto operator()(blank_size)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
struct is_assigned_dim
{
    template<typename... T>
    constexpr auto operator()(assigned_dim<T...>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
}

template<typename T>
concept StaticSize = static_cast<bool>(std::invoke_result_t<detail::is_static_size, T>{});

template<typename T>
concept ArbitrarySize = static_cast<bool>(std::invoke_result_t<detail::is_arbitrary_size, T>{});

template<typename T>
concept DynamicSize = static_cast<bool>(std::invoke_result_t<detail::is_dynamic_size, T>{});

template<typename T>
concept BlankSize = static_cast<bool>(std::invoke_result_t<detail::is_blank_size, T>{});

template<typename T>
concept SizeType = StaticSize<T> || ArbitrarySize<T> || DynamicSize<T> || BlankSize<T>;

template<typename T>
concept StaticOrArbitrarySize = StaticSize<T> || ArbitrarySize<T>;

template<typename T>
concept AssignedDim = static_cast<bool>(std::invoke_result_t<detail::is_assigned_dim, T>{});

template <DimensionType D, SizeType S>
struct extent;

namespace detail
{
struct is_extent
{
    template <typename N, typename T>
    constexpr auto operator()(extent<N, T>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};

struct is_dynamic_extent
{
    template <typename N>
    constexpr auto operator()(extent<N, dynamic_size>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};

template<typename T>
struct is_extend_with_name
{
    template <typename S>
    constexpr auto operator()(extent<T, S>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
}

constexpr auto extent_predicate = [](Type t)
{
    return value(invoke_result(type<detail::is_extent>, t));
};


template<typename N>
constexpr auto extend_with_name_predicate = [](Type t)
{
    return value(invoke_result(type<detail::is_extend_with_name<N>>, t));
};

template <typename T>
concept Extent = static_cast<bool>(value(invoke_result(type<detail::is_extent>, type<T>)));

template <DimensionType D, SizeType S>
struct extent {
    using DimensionType = D;
    using Name = typename D::dimension_name;
    using SizeType = S;
    D dimension;
    S size;
    constexpr extent(const D &dimension, const S &size) : dimension(dimension), size(size) {};
    constexpr extent(D &&dimension, S &&size) : dimension(std::forward<D>(dimension)), size(std::forward<S>(size)) {};
    constexpr extent(const S &size) : dimension(D{}), size(size) {};
    constexpr extent(S &&size) : dimension(D{}), size(std::forward<S>(size)) {};
    constexpr extent() : dimension(D{}), size(S{}) {};
    constexpr extent(const extent<D,S> & e) : dimension(D{}), size(e.size) {};
    constexpr extent(const std::tuple<> &) : dimension(D{}), size(S{}) {};
    
    constexpr std::size_t getSize() const
    {
        return size.size;
    }
};

struct incompatible_sizes_exception : public std::exception
{
   const char * what () const throw ()
   {
      return "Combine called with incompatible sizes.";
   }
};

namespace detail
{
    template<SizeType T>
    constexpr auto combine_single(const T & t, const blank_size &) {
        return t;
    }
    template<SizeType T>
    constexpr auto combine_single(const blank_size &, const T & t) {
        return t;
    }
    template<SizeType T>
        requires !std::is_same_v<T, blank_size>
    constexpr auto combine_single(const T & t, const arbitrary_size &) {
        return t;
    }
    template<SizeType T>
        requires !std::is_same_v<T, blank_size>
    constexpr auto combine_single(const arbitrary_size &, const T & t) {
        return t;
    }
    template<std::size_t S1, std::size_t S2>
    constexpr auto combine_single(const static_size<S1> &, const static_size<S2> &) {
        static_assert(S1 == S2, "Incompatible sizes!");
        return static_size<S1>();
    }
    template<std::size_t S>
    constexpr auto combine_single(const static_size<S> &, const dynamic_size & r) {
        if (r.size != S)
        {
            throw incompatible_sizes_exception();
        }
        return static_size<S>();
    }
    template<std::size_t S>
    constexpr auto combine_single(const dynamic_size & l, const static_size<S> &) {
        if (l.size != S)
        {
            throw incompatible_sizes_exception();
        }
        return static_size<S>();
    }
    constexpr auto combine_single(const dynamic_size & l, const dynamic_size & r) {
        if (l.size != r.size)
        {
            throw incompatible_sizes_exception();
        }
        return dynamic_size(l);
    }

    template<typename D1, typename S1, typename D2, typename S2>
    constexpr auto combine_single(const extent<D1, S1> & e1, const extent<D2, S2> & e2) {
        static_assert(std::is_same_v<D1, D2>, "Wrong usage of combine_single. Extents must have the same dimension name.");
        return extent(e1.dimension, combine_single(e1.size, e2.size));
    }

    struct is_tuple_of_const_extent_ref
    {
        template <Extent... T>
        constexpr auto operator()(std::tuple<const T & ...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_tuple_of_extent
    {
        template <Extent... T>
        constexpr auto operator()(std::tuple<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
}
template <typename T>
concept TupleOfExtents = static_cast<bool>(value(invoke_result(type<detail::is_tuple_of_extent>, type<T>)));

namespace detail
{
template<typename... T, typename E, int I, int S>
constexpr auto replace(const std::tuple<T...> & t, const E & e, constant_<I>, constant_<S>)
{
    if constexpr (sizeof...(T) == I)
    {
        return std::tuple<>();
    }
    else if constexpr (I == S)
    {
        return std::tuple_cat(std::make_tuple(e),
            replace(t, e, constant<I+1>, constant<S>));
    }
    else
    {
        return std::tuple_cat(std::tuple<std::tuple_element_t<I, std::tuple<T...>>>(std::get<I>(t)),
            replace(t, e, constant<I+1>, constant<S>));
    }
}

template<Extent... B, Extent T, int S>
constexpr auto addNewExtentToResult(const std::tuple<B...> & browsed, const T & t, constant_<S>)
{
    if constexpr (sizeof...(B) == 0 || sizeof...(B) == S)
    {
        return std::tuple_cat(browsed, std::make_tuple(t));
    }
    else if constexpr (std::is_same_v<
        typename std::tuple_element_t<S, std::tuple<B...>>::Name,
        typename T::Name>)
    {
        auto resultOfCombine = combine_single(std::get<S>(browsed), t);
        return replace(browsed, resultOfCombine, constant<0>, constant<S>);
    }
    else
    {
        return addNewExtentToResult(browsed, t, constant<S+1>);
    }
}
template<Extent... T>
constexpr auto combineExtentsAux(const std::tuple<T...> & t)
{
    return t;
}
template<Extent... E, Extent T, Extent... Ts>
constexpr auto combineExtentsAux(const std::tuple<E...> & e, T & t, const Ts & ... ts)
{
    return combineExtentsAux(addNewExtentToResult(e, t, constant<0>), ts...);
}
}

template<Extent... Ts>
constexpr auto combineExtents(const Ts & ... ts)
{
    return detail::combineExtentsAux(std::tuple<>(), ts...);
}
template<typename... T>
constexpr auto combine(const std::tuple<T...> & t)
{
    constexpr auto lambda = [](const T & ... t){ return combineExtents(t...); };
    return std::apply(lambda, t);
}
template<typename... T>
struct CombineExtents
{
   static_assert(always_false_v<T...>);
};
template<typename... T>
struct CombineExtents<std::tuple<T...>>
{
    static constexpr auto lambda = [](const T & ... t){ return combineExtents(t...); };
    using type = std::invoke_result_t<decltype(lambda), T...>;
};

namespace detail
{

struct is_dimension_type
{
    constexpr auto operator()(DimensionType)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};

template <typename... T>
struct insert { static_assert(always_false_v<T...>); };
template <typename... T>
struct merge_type_list { static_assert(always_false_v<T...>); };
template <typename... T>
struct merge_dimension_list { static_assert(always_false_v<T...>); };
template <typename... T>
struct merge_dimension_list_aux { static_assert(always_false_v<T...>); };


template <typename... A, typename B>
struct insert<std::tuple<A...>, std::tuple<>, B>
{
    using type = std::tuple<A..., B>;
};
template <typename... A, typename B, typename... C, typename D>
struct insert<std::tuple<A...>, std::tuple<B, C...>, D>
{
    static_assert(std::is_same_v<B, D> == false);
    using type = insert<std::tuple<A..., B>, std::tuple<C...>, D>::type;
};


template <typename... A, typename B, typename... C>
struct merge_type_list<std::tuple<A...>, B, C...>
{
    using type = merge_type_list<
        typename insert<std::tuple<>, std::tuple<A...>, B>::type,
        C...
    >::type;
};
template <typename... A>
struct merge_type_list<std::tuple<A...>>
{
    using type = std::tuple<A...>;
};


template <typename... T>
struct merge_dimension_list_aux<std::tuple<T...>>
{
    using type = dimension_list<T...>;
};


template <typename... A, typename... B>
struct merge_dimension_list<dimension_list<A...>, dimension_list<B...>>
{
    using type = merge_dimension_list_aux<
        typename merge_type_list<std::tuple<A...>, B...>::type
    >::type;
};

}

template <DimensionName... Ns>
struct dimension_list
{
    constexpr dimension_list() = default;
    constexpr dimension_list(const dimension_list<Ns...> &) = default;

    friend constexpr bool operator==(dimension_list, dimension_list)
    {
        return true;
    }

    template <typename... N2>
    friend constexpr bool operator==(dimension_list, dimension_list<N2...>)
    {
        return false;
    }

    template <typename... N2>
    friend constexpr bool operator!=(dimension_list d1, dimension_list<N2...> d2)
    {
        return not (d1 == d2);
    }

    template <typename... Ts>
    constexpr auto operator,(const dimension_list<Ts...> &)
    {
        using return_type = typename detail::merge_dimension_list<dimension_list<Ns...>, dimension_list<Ts...>>::type;
        return return_type();
    }

    template <typename T>
    constexpr auto operator=(T && value) const
    {
        return assigned_dim<dimension_list<Ns...>, T>{ std::forward<T>(value) };
    }

    template<typename T, size_t S1, size_t S2>
    constexpr auto operator=(T (&&value)[S1][S2]) const
        requires sizeof...(Ns) == S2
    {
        std::array<std::array<T, S2>, S1> result{};
        for (size_t i = 0; i < S1; ++i)
        {
            for (size_t j = 0; j < S2; ++j)
            {
                result[i][j] = std::forward<T>(value[i][j]);
            }
        }
        return assigned_dim<dimension_list<Ns...>, std::array<std::array<T, S2>, S1>> { std::move(result) };
    }
};

template <DimensionName N = decltype([]{})>
struct dimension : dimension_list<N>
{
    using dimension_name = N;
    using Name = N;
    constexpr dimension() = default;
    constexpr dimension(const dimension<N> &) {};

    constexpr auto operator~()
    {
        return extent<dimension<N>, arbitrary_size>();
    }

    template <Dimension D>
    constexpr auto operator=(const D & value) const
    {
        return assigned_dim<dimension<N>, D>{};
    }
    
    template <typename T>
        requires !Dimension<T>
    constexpr auto operator=(const T & value) const
    {
        return assigned_dim<dimension<N>, T>(value);
    }

    template<typename T, size_t S1>
    constexpr auto operator=(T (&&value)[S1]) const
    {
        std::array<T,S1> result{};
        for (size_t i = 0; i < S1; ++i)
        {
            result[i] = std::forward<T>(value[i]);
        }
        return assigned_dim<dimension<N>, std::array<T,S1>>{ std::move(result) };
    }
};

template <DimensionName N = decltype([]{})>
struct anonymous_dimension : dimension<N>
{
    using dimension_name = N;
    using Name = N;
    constexpr anonymous_dimension(const anonymous_dimension<N> &) {};
    constexpr anonymous_dimension() = default;

    constexpr auto operator~()
    {
        return extent<anonymous_dimension<N>, arbitrary_size>();
    }

    template <Dimension D>
    constexpr auto operator=(const D & value) const
    {
        return assigned_dim<anonymous_dimension<N>, D>{};
    }

    template <typename T>
        requires !Dimension<T>
    constexpr auto operator=(T && value) const
    {
        return assigned_dim<anonymous_dimension<N>, T>{ std::forward<T>(value) };
    }

    template<typename T, size_t S1>
    constexpr auto operator=(T (&&value)[S1]) const
    {
        std::array<T,S1> result{};
        for (size_t i = 0; i < S1; ++i)
        {
            result[i] = std::forward<T>(value[i]);
        }
        return assigned_dim<anonymous_dimension<N>, std::array<T,S1>>{ std::move(result) };
    }
};

}
