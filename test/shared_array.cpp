#include "gtest/gtest.h"
#include "dime/dynamic_array.hpp"
#include "dime/static_array.hpp"
#include "dime/extent.hpp"
#include "dime/types.hpp"

#include <iostream>
#include <array>
#include <vector>

namespace {

using namespace dime;

TEST(DynamicArray, Constructor) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    shared_array a(x=2_c);
    shared_array b(x=3);
    shared_array c(x=1_c, y=2, z=4);
    shared_array d(~z);
    std::vector<int> v = std::vector<int>(5);
    static_assert(std::is_same_v<decltype(shared_array(x,v)), decltype(shared_array(v, x))>);
    shared_array e(x, v);
    std::vector<std::vector<int>> u = std::vector<std::vector<int>>(5, std::vector<int>(3));
    shared_array f(u);
    shared_array g(x, u, y);
    shared_array h(z, u);
}

TEST(DynamicArray, CopyConstructor) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    std::vector<int> source = {{0,1,2,3}};
    std::array<int, 5> static_source = {{0,1,2,3}};

    shared_array a(x, source);
    EXPECT_EQ(a.at(x=0), 0);
    EXPECT_EQ(a.at(x=1), 1);
    EXPECT_EQ(a.at(x=2), 2);
    EXPECT_EQ(a.at(x=3), 3);

    shared_array b(a);
    EXPECT_EQ(b.at(x=0), 0);
    EXPECT_EQ(b.at(x=1), 1);
    EXPECT_EQ(b.at(x=2), 2);
    EXPECT_EQ(b.at(x=3), 3);

    b.at(x=2) = 42;
    EXPECT_EQ(a.at(x=2), 42);
    EXPECT_EQ(b.at(x=2), 42);

    static_array c(x, static_source);
    EXPECT_EQ(c.at(x=0), 0);
    EXPECT_EQ(c.at(x=1), 1);
    EXPECT_EQ(c.at(x=2), 2);
    EXPECT_EQ(c.at(x=3), 3);

    shared_array d(c);
    EXPECT_EQ(d.at(x=0), 0);
    EXPECT_EQ(d.at(x=1), 1);
    EXPECT_EQ(d.at(x=2), 2);
    EXPECT_EQ(d.at(x=3), 3);

    shared_array e(std::move(d));
    EXPECT_EQ(e.at(x=0), 0);
    EXPECT_EQ(e.at(x=1), 1);
    EXPECT_EQ(e.at(x=2), 2);
    EXPECT_EQ(e.at(x=3), 3);

    dynamic_array f(x, static_source);
    EXPECT_EQ(f.at(x=0), 0);
    EXPECT_EQ(f.at(x=1), 1);
    EXPECT_EQ(f.at(x=2), 2);
    EXPECT_EQ(f.at(x=3), 3);

    shared_array g(f);
    EXPECT_EQ(g.at(x=0), 0);
    EXPECT_EQ(g.at(x=1), 1);
    EXPECT_EQ(g.at(x=2), 2);
    EXPECT_EQ(g.at(x=3), 3);
}

TEST(DynamicArray, At) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    shared_array a(x=2_c);
    a.at(x=0) = 2;
    a.at(x=1) = 3;
    EXPECT_EQ(a.at(x=0), 2);
    EXPECT_EQ(a.at(x=1), 3);
    EXPECT_EQ(a.at(x=1_c), 3);
    
    shared_array b(x=3);
    b.at(x=0) = 2;
    b.at(x=2_c) = 4;
    EXPECT_EQ(b.at(x=0), 2);
    EXPECT_EQ(b.at(x=2), 4);
    EXPECT_EQ(b.at(x=2_c), 4);
    
    shared_array c(x=1_c, y=2, z=4);
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 4; j++) {
            c(x=0, y=i, z=j) = i + j*2;
        }
    }
    bool match = true;
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 4; j++) {
            match = match && (c.at(x=0, y=i, z=j) == i + j*2);
        }
    }
    EXPECT_TRUE(match);
    
    shared_array d(~z);
    d.at(z=100) = 5;
    d.at(z=24_c) = 42;
    EXPECT_EQ(d.at(z=0), 42);
}

}