#pragma once

#include "types.hpp"
#include "dimensions.hpp"
#include "extent.hpp"
#include "dim_facet.hpp"
#include "strides_facet.hpp"
#include "element_type_facet.hpp"
#include "data_facet.hpp"
#include "utils.hpp"
#include "array_view.hpp"

#include <array>
#include <tuple>
#include <type_traits>

namespace dime
{

struct static_array_factory
{
    constexpr auto operator ()(DimensionType dimension)
    {
        return dimension;
    }

    constexpr auto operator ()(ElementType elementType)
    {
        return elementType;
    }

    constexpr auto operator ()(DeviceType deviceType)
    {
        return deviceType;
    }

    template<typename N, typename S>
        requires static_cast<bool>(std::invoke_result_t<detail::is_static_size, S>{})
            || static_cast<bool>(std::invoke_result_t<detail::is_arbitrary_size, S>{})
    constexpr auto operator ()(extent<N, S> extent)
    {
        return extent;
    }

    template<Constant T, typename N>
    constexpr auto operator ()(assigned_dim<N, T> assigned_dim)
    {
        using Size = static_size<T::value>;
        return extent(typename std::remove_reference_t<N>(), Size{});
    }

    template<typename T, std::size_t S>
    constexpr auto operator ()(std::array<T,S> ds)
    {
        return std::make_tuple(
            data(ds),
            shape(getSTLConatinerShape(ds)),
            element_type<typename getSTLConatinerDataType<std::array<T,S>>::type>);
    }

    template<typename... T>
    auto operator ()(std::tuple<T...> t)
    {
        static_assert((Extent<T> && ...));
        return t;
    }

    template<typename... T>
    auto operator ()(const expression<T...> & e)
    {
        return std::tuple_cat(std::forward_as_tuple(e, e.getElementType()), e.outer());
    }

    template<typename... T>
    auto operator ()(const array_view<T...> & e)
    {
        return std::tuple_cat(std::forward_as_tuple(e, element_type<typename array_view<T...>::ElementType>), e.outer());
    }

    template<typename... T>
    auto operator ()(const dynamic_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<const T &>(e)...);
    }
    template<typename... T>
    auto operator ()(dynamic_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<T &>(e)...);
    }
    template<typename... T>
    auto operator ()(dynamic_array<T...> && e)
    {
        return std::forward_as_tuple(static_cast<T &&>(e)...);
    }

    template<typename... T>
    auto operator ()(const shared_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<const T &>(e)...);
    }
    template<typename... T>
    auto operator ()(shared_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<T &>(e)...);
    }
    template<typename... T>
    auto operator ()(shared_array<T...> && e)
    {
        return std::forward_as_tuple(static_cast<T &&>(e)...);
    }
    
    template<typename... T>
    auto operator ()(const static_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<const T &>(e)...);
    }
    template<typename... T>
    auto operator ()(static_array<T...> & e)
    {
        return std::forward_as_tuple(static_cast<T &>(e)...);
    }
    template<typename... T>
    auto operator ()(static_array<T...> && e)
    {
        return std::forward_as_tuple(static_cast<T &&>(e)...);
    }
};

template <typename... Facets>
struct static_array : form<static_array_factory, Facets...>
{
    using form<static_array_factory, Facets...>::form;
    template<typename... T>
    constexpr static_array(const expression<T...> & e) : form<static_array_factory, Facets...>(e)
    {
    };
    template<typename... T>
    constexpr static_array(const array_view<T...> & e) : form<static_array_factory, Facets...>(e)
    {
    };

    template<typename... T>
        requires std::tuple_size_v<typename std::tuple_element_t<0, std::tuple<Facets...>>::Outer> == sizeof...(T)
            && !(std::tuple_size_v<typename index_converter<T>::type::Outer> || ...)
    constexpr auto & operator() (const T & ... t)
    {
        return this->at(t...);
    }
    template<typename... T>
        requires !(std::tuple_size_v<typename std::tuple_element_t<0, std::tuple<Facets...>>::Outer> == sizeof...(T)
            && !(std::tuple_size_v<typename index_converter<T>::type::Outer> || ...))
    constexpr auto operator() (const T & ... t)
    {
        return array_view(*this, t...);
    }

    template<typename... Ts>
    const auto & at(std::tuple<Ts...> const & t) const {
        return this->access(this->getTotalOffsetTuple(t));
    }
    template<typename... Ts>
    constexpr auto & at(std::tuple<Ts...> const & t) {
        return this->access(this->getTotalOffsetTuple(t));
    }
    template<typename... Ts>
    const auto & at(const Ts & ... ts) const {
        return this->access(this->getTotalOffset(ts...));
    }    
    template<typename... Ts>
    constexpr auto & at(const Ts & ... ts) {
        return this->access(this->getTotalOffset(ts...));
    }
};

template <typename... Initializers>
static_array(Initializers...) -> static_array<
    dim_facet_from<features_from<static_array_factory, Initializers...>>,
    strides_facet_from<
        dim_facet_from<features_from<static_array_factory, Initializers...>>,
        features_from<static_array_factory, Initializers...>
    >,
    element_type_facet_from<features_from<static_array_factory, Initializers...>>,
    data_facet_from<
        StaticData,
        element_type_facet_from<features_from<static_array_factory, Initializers...>>,
        dim_facet_from<features_from<static_array_factory, Initializers...>>>
>;
template<typename... T>
static_array(const expression<T...> &) -> static_array<
    dim_facet_from<features_from<static_array_factory, expression<T...>>>,
    strides_facet_from<
        dim_facet_from<features_from<static_array_factory, expression<T...>>>,
        features_from<static_array_factory, expression<T...>>
    >,
    element_type_facet_from<features_from<static_array_factory, expression<T...>>>,
    data_facet_from<
        StaticData,
        element_type_facet_from<features_from<static_array_factory, expression<T...>>>,
        dim_facet_from<features_from<static_array_factory, expression<T...>>>>
>;
template<typename... T>
static_array(const array_view<T...> &) -> static_array<
    dim_facet_from<features_from<static_array_factory, array_view<T...>>>,
    strides_facet_from<
        dim_facet_from<features_from<static_array_factory, array_view<T...>>>,
        features_from<static_array_factory, array_view<T...>>
    >,
    element_type_facet_from<features_from<static_array_factory, array_view<T...>>>,
    data_facet_from<
        StaticData,
        element_type_facet_from<features_from<static_array_factory, array_view<T...>>>,
        dim_facet_from<features_from<static_array_factory, array_view<T...>>>>
>;

template <typename T, auto... Initializers>
concept StaticArrayForm = Form<T, static_array_factory, Initializers...>;

}
