#pragma once

#include "types.hpp"
#include "constants.hpp"

#include <tuple>
#include <type_traits>

namespace dime
{

template <typename T>
struct element_type_
{
    using type = T;
};

template <typename T>
constexpr auto element_type = element_type_<T>{};

template <typename T>
struct element_type_facet
{
    using ElementType = T;
    using type = T;
    
    constexpr element_type_facet(Features const & fs) {}
    constexpr element_type_facet(element_type_facet<T> const & e) {}
};

template<typename T>
concept ElementTypeFacet = static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<element_type_facet>, T>{});

namespace detail {
    struct is_element_type
    {
        template<typename T>
        constexpr auto operator()(element_type_<T>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };

    constexpr auto element_type_feature_predicate = [](Type t)
    {
        return std::invoke_result_t<dime::detail::is_element_type, typename decltype(t)::type>{};
    };

    template<typename T>
    struct element_type_features_to_element_type_facet
    {
        static_assert(always_false_v<T>, "Incorrect number of element type.");
    };
    template<>
    struct element_type_features_to_element_type_facet<std::tuple<>>
    {
        using type = element_type_facet<int>;
    };
    template<typename T>
    struct element_type_features_to_element_type_facet<std::tuple<T>>
    {
        using type = element_type_facet<typename std::remove_reference_t<T>::type>;
    };
}

template<typename T>
concept ElementType = static_cast<bool>(value(invoke_result(type<detail::is_element_type>, type<T>)));

template <Features F>
using element_type_facet_from = typename detail::element_type_features_to_element_type_facet<std::invoke_result_t<F, decltype(detail::element_type_feature_predicate)>>::type;

}
