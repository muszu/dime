#include "gtest/gtest.h"
#include "dime/constants.hpp"
#include "dime/sequences.hpp"

namespace {

using namespace dime::ypo;

TEST(Sequences, Equality) {
    static_assert(sequence<2,1> != sequence<1,2>);
    static_assert(range_sequence<5> == sequence<0,1,2,3,4>);
    static_assert(range_sequence<7>.filter([](Constant i){ return i % 2_c == 0_c; }) == sequence<0,2,4,6>);
}

std::true_type check(Sequence);
std::false_type check(...);

TEST(Sequences, SequenceConcept) {
    static_assert(decltype(check(range_sequence<5>)){});
    static_assert(not decltype(check(5_c)){});
}

}