#pragma once

#include "utils.hpp"
#include "types.hpp"
#include "constants.hpp"
#include "dimensions.hpp"
#include "dynamic_array.hpp"
#include "static_array.hpp"
 
#include <tuple>
#include <type_traits>

namespace dime
{

namespace detail
{
    template<typename... T>
    struct array_with_dims_contains_aux
    {
        static_assert(always_false_v<T...>);
    };
    template<typename AD, typename T, typename... Ts>
    struct array_with_dims_contains_aux<AD, std::tuple<T, Ts...>>
    {
        static constexpr bool value = std::is_same_v<std::remove_reference_t<std::remove_cv_t<AD>>, std::remove_reference_t<std::remove_cv_t<T>>>
            || array_with_dims_contains_aux<AD, std::tuple<Ts...>>::value;
    };
    template<typename AD>
    struct array_with_dims_contains_aux<AD, std::tuple<>>
    {
        static constexpr bool value = false;
    };
    template<typename... T>
    struct array_with_dims_aux { static_assert(always_false_v<T...>);};
    template<size_t... I, typename... T>
    struct array_with_dims_aux<sequence_<I...>, T...>
    {
        static_assert(Array<std::tuple_element_t<sizeof...(T)-1, std::tuple<T...>>>, "Last type should be an array.");
        static_assert(sizeof...(I) == sizeof...(T), "Incorrect call.");
        static constexpr size_t size = sizeof...(T);
        static constexpr bool value_correct_type = ((
            I == size - 1 ? Array<std::tuple_element_t<I, std::tuple<T...>>> : (AssignedDim<std::tuple_element_t<I, std::tuple<T...>>> || Extent<std::tuple_element_t<I, std::tuple<T...>>>)
        ) && ...);
        static constexpr bool value_contains = ((
            I == size - 1
                ? true
                : array_with_dims_contains_aux<
                    std::invoke_result_t<dynamic_array_factory, std::tuple_element_t<I, std::tuple<T...>>>,
                    typename std::tuple_element_t<size - 1, std::tuple<T...>>::Outer>::value
        ) && ...);
        static constexpr bool value_length_match = std::tuple_size<typename std::tuple_element_t<size - 1, std::tuple<T...>>::Outer>::value == size - 1;
        static constexpr bool value = value_correct_type && value_contains && value_length_match;
    };
}

template<typename... T>
concept ArrayWithDims = detail::array_with_dims_aux<range_sequence_<sizeof...(T)>, T...>::value;
template<AssignedDim T1, typename U>
concept ArrayWith1Dim = ArrayWithDims<T1,U>;
template<AssignedDim T1, AssignedDim T2, typename U>
concept ArrayWith2Dims = ArrayWithDims<T1,T2,U>;
template<AssignedDim T1, AssignedDim T2, AssignedDim T3, typename U>
concept ArrayWith3Dims = ArrayWithDims<T1,T2,T3,U>;
template<AssignedDim T1, AssignedDim T2, AssignedDim T3, AssignedDim T4, typename U>
concept ArrayWith4Dims = ArrayWithDims<T1,T2,T3,T4,U>;
template<AssignedDim T1, AssignedDim T2, AssignedDim T3, AssignedDim T4, AssignedDim T5, typename U>
concept ArrayWith5Dims = ArrayWithDims<T1,T2,T3,T4,T5,U>;
}