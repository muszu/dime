#include "gtest/gtest.h"
#include "dime/dynamic_array.hpp"
#include "dime/static_array.hpp"
#include "dime/extent.hpp"
#include "dime/types.hpp"
#include "dime/expression.hpp"

#include <iostream>
#include <array>
#include <vector>

namespace {

using namespace dime;

TEST(Expression, Building) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dynamic_array a(x=2_c);
    dynamic_array b(x=2_c);
    dynamic_array c(x=2_c, y=3_c, z=4);
    dynamic_array d(~z);
    dynamic_array h(x=2, ~y, z=4_c);
    
    auto e0 = a * b;
    EXPECT_TRUE((std::is_same_v<
        typename decltype(e0)::Outer,
        std::tuple<dime::extent<dime::dimension<int>, dime::static_size<2> > >>));
    
    auto e1 = a*b*c;
    EXPECT_TRUE((std::is_same_v<
        typename decltype(e1)::Outer,
        std::tuple<
            dime::extent<dime::dimension<int>, dime::static_size<2> >,
            dime::extent<dime::dimension<bool>, dime::static_size<3> >,
            dime::extent<dime::dimension<char>, dime::dynamic_size> >>));
    
    auto e2 = a+b*c;
    EXPECT_TRUE((std::is_same_v<
        typename decltype(e2)::Outer,
        std::tuple<
            dime::extent<dime::dimension<int>, dime::static_size<2> >,
            dime::extent<dime::dimension<bool>, dime::static_size<3> >,
            dime::extent<dime::dimension<char>, dime::dynamic_size> >>));
    EXPECT_TRUE((std::is_same_v<typename decltype(e2)::ResultElementType, int>));

    auto e3 = a&c;
    EXPECT_TRUE((std::is_same_v<
        typename decltype(e3)::Outer,
        std::tuple<dime::extent<dime::dimension<int>, dime::static_size<2> >, dime::extent<dime::dimension<bool>, dime::static_size<3> >, dime::extent<dime::dimension<char>, dime::dynamic_size> >>));
    
    auto e4 = a%4;
    EXPECT_TRUE((std::is_same_v<
        typename decltype(e4)::Outer,
        std::tuple<dime::extent<dime::dimension<int>, dime::static_size<2> > >>));

    auto e5 = !a;
    EXPECT_TRUE((std::is_same_v<
        typename decltype(e5)::Outer,
        std::tuple<dime::extent<dime::dimension<int>, dime::static_size<2> > >>));

    auto e6 = c * h;
    auto e6_outer = e6.outer();
    EXPECT_EQ(get<0>(e6_outer).size.size, 2);
    EXPECT_EQ(get<1>(e6_outer).size.size, 3);
    EXPECT_EQ(get<2>(e6_outer).size.size, 4);
}


TEST(Expression, ResultInfo) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dynamic_array a(x=2_c, y=3_c);
    dynamic_array b(y=3_c, z=2_c);
    dynamic_array c(y=3, z=2_c);
    dynamic_array d(~y, z=2_c);

    auto e1 = a * b;
    auto size_info = e1.outer();
    auto element_info = e1.getElementType();
    dynamic_array de1(size_info, element_info);
    dynamic_array de2(e1);
    dynamic_array de3(a * b);
    EXPECT_TRUE((std::is_same_v<decltype(de1), decltype(de2)>));

    EXPECT_TRUE((std::is_same_v<decltype(de2), decltype(de3)>));
    EXPECT_TRUE((std::is_same_v<
        typename decltype(de1)::Outer,
        std::tuple<
            dime::extent<dime::dimension<int>, dime::static_size<2> >,
            dime::extent<dime::dimension<bool>, dime::static_size<3> >,
            dime::extent<dime::dimension<char>, dime::static_size<2> > > >));
    
    static_array s1(a * b);
    EXPECT_TRUE((std::is_same_v<typename decltype(de1)::Outer, typename decltype(s1)::Outer>));
    
    dynamic_array da1 = (a * b);
    dynamic_array da2 = e1;
    EXPECT_TRUE((std::is_same_v<typename decltype(da1)::Outer, typename decltype(da2)::Outer>));
    static_array sa1 = (a * b);
    EXPECT_TRUE((std::is_same_v<typename decltype(da2)::Outer, typename decltype(sa1)::Outer>));
    static_array sa2 = e1;
    EXPECT_TRUE((std::is_same_v<typename decltype(sa1)::Outer, typename decltype(sa2)::Outer>));
}

TEST(Expression, CorrectOutputData) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    
    static_array s1(x=4_c);
    static_array s2(y=4_c);

    auto s1_mul_s2 = s1 * s2;
    
    for(int i = 0; i < 4; i++) {
        s1.at(x=i) = i+1;
        s2.at(y=i) = 2 << i;
    }
    
    static_array s3 = s1_mul_s2;

    bool match = true;
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 4; j++) {
            match = match && (s3.at(x=i, y=j) == (i+1) * (2 << j));
        }
    }
    EXPECT_TRUE(match);

    dynamic_array d1(z=2);
    d1.at(z=0) = 1;
    d1.at(z=1) = 2;
    dynamic_array d2 = d1 * d1;
    EXPECT_EQ(d2.at(z=0), 1);
    EXPECT_EQ(d2.at(z=1), 4);
    int i3 = 3;
    auto e1 = d1 + i3;
    EXPECT_EQ(e1.at(std::make_tuple(z=0)), 4);
    EXPECT_EQ(e1.at(std::make_tuple(z=1)), 5);
    dynamic_array d3 = 1 + d1 + 3;
    EXPECT_EQ(d3.at(std::make_tuple(z=0)), 5);
    EXPECT_EQ(d3.at(std::make_tuple(z=1)), 6);
}

TEST(Expression, Indexing) {
    dimension<int> x;
    dimension<bool> y;

    static_array s1(x=4_c);
    static_array s2(y=4_c);
    int a = 1;

    for(int i = 0; i < 4; i++) {
        s1.at(x=i) = i;
        s2.at(y=i) = (i+1)/2;
    }
    EXPECT_EQ(s1.at(x=0), 0);
    EXPECT_EQ(s1.at(x=1), 1);
    EXPECT_EQ(s1.at(x=2), 2);
    EXPECT_EQ(s1.at(x=3), 3);
    
    EXPECT_EQ(s2.at(y=0), 0);
    EXPECT_EQ(s2.at(y=1), 1);
    EXPECT_EQ(s2.at(y=2), 1);
    EXPECT_EQ(s2.at(y=3), 2);

    auto e1 = s1 * a;
    EXPECT_EQ(e1.at(x=0), 0);
    EXPECT_EQ(e1.at(x=1), 1);
    EXPECT_EQ(e1.at(x=2), 2);
    EXPECT_EQ(e1.at(x=3), 3);

    array_view s3 = s2(y=e1);
    EXPECT_EQ(s3.at(x=0), 0);
    EXPECT_EQ(s3.at(x=1), 1);
    EXPECT_EQ(s3.at(x=2), 1);
    EXPECT_EQ(s3.at(x=3), 2);

    array_view s4 = s1(x=s2 + a);
    EXPECT_EQ(s4.at(y=0), 1);
    EXPECT_EQ(s4.at(y=1), 2);
    EXPECT_EQ(s4.at(y=2), 2);
    EXPECT_EQ(s4.at(y=3), 3);
}

TEST(Expression, Add) {
    dimension<int> x;
    dimension<bool> y;

    static_array s1(x=4_c);
    static_array s2(y=4_c);

    for(int i = 0; i < 4; i++) {
        s1.at(x=i) = i;
        s2.at(y=i) = 1;
    }
    EXPECT_EQ(add(s1, x), 6);
    EXPECT_EQ(add(s2, y), 4);

    static_array s3 = s1 * s2;
    static_array s4 = add(s3, y);
    EXPECT_EQ(s4.at(x=0), 0);
    EXPECT_EQ(s4.at(x=1), 4);
    EXPECT_EQ(s4.at(x=2), 8);
    EXPECT_EQ(s4.at(x=3), 12);

    int s5 = add(s4, x);
    EXPECT_EQ(s5, 24);
    int s6 = add(add(s1 * s2, y), x);
    EXPECT_EQ(s6, 24);
    int s7 = add(s1 * s2, y, x);
    EXPECT_EQ(s7, 24);
}

TEST(Expression, Mul) {
    dimension<int> x;
    dimension<bool> y;

    static_array s1(x=4_c);
    static_array s2(y=4_c);

    for(int i = 0; i < 4; i++) {
        s1.at(x=i) = i+1;
        s2.at(y=i) = 1;
    }
    EXPECT_EQ(mul(s1, x), 24);
    EXPECT_EQ(mul(s2, y), 1);

    static_array s3 = s1 * s2;
    static_array s4 = mul(s3, y);
    EXPECT_EQ(s4.at(x=0), 1);
    EXPECT_EQ(s4.at(x=1), 16);
    EXPECT_EQ(s4.at(x=2), 81);
    EXPECT_EQ(s4.at(x=3), 256);

    int s5 = mul(s4, x);
    EXPECT_EQ(s5, 331776);
    int s6 = mul(mul(s1 * s2, y), x);
    EXPECT_EQ(s6, 331776);
    int s7 = mul(s1 * s2, y, x);
    EXPECT_EQ(s7, 331776);
}

TEST(Expression, MinMax) {
    dimension<int> x;
    dimension<bool> y;

    static_array s1(x=4_c);
    static_array s2(y=4_c);

    for(int i = 0; i < 4; i++) {
        s1.at(x=i) = i+1;
        s2.at(y=i) = -1;
    }
    s2.at(y=2) = 42;

    EXPECT_EQ(min(s1, x), 1);
    EXPECT_EQ(min(s2, y), -1);
    EXPECT_EQ(max(s1, x), 4);
    EXPECT_EQ(max(s2, y), 42);

    static_array s3 = s1 * s2;
    static_array s4 = max(s3, y);
    EXPECT_EQ(s4.at(x=0), 42);
    EXPECT_EQ(s4.at(x=1), 84);
    EXPECT_EQ(s4.at(x=2), 126);
    EXPECT_EQ(s4.at(x=3), 168);

    static_array s5 = min(s3, y);
    EXPECT_EQ(s5.at(x=0), -1);
    EXPECT_EQ(s5.at(x=1), -2);
    EXPECT_EQ(s5.at(x=2), -3);
    EXPECT_EQ(s5.at(x=3), -4);

    EXPECT_EQ(max(s3, x, y), 168);
    EXPECT_EQ(min(s3, x, y), -4);
}


TEST(Expression, Misc) {
    dimension<int> x;
    dimension<bool> y;

    static_array s1(x=4_c);
    static_array s2(y=4_c);

    for(int i = 0; i < 4; i++) {
        s1.at(x=i) = i;
        s2.at(y=i) = -1;
    }
    s2.at(y=2) = 1;

    static_array s3 = s1 > 2;
    EXPECT_EQ(s3.at(x=0), false);
    EXPECT_EQ(s3.at(x=1), false);
    EXPECT_EQ(s3.at(x=2), false);
    EXPECT_EQ(s3.at(x=3), true);
    
    static_array s4 = 0 < s2;
    EXPECT_EQ(s4.at(y=0), false);
    EXPECT_EQ(s4.at(y=1), false);
    EXPECT_EQ(s4.at(y=2), true);
    EXPECT_EQ(s4.at(y=3), false);

    static_array s5 = !s1;
    EXPECT_EQ(s5.at(x=0), true);
    EXPECT_EQ(s5.at(x=1), false);
    EXPECT_EQ(s5.at(x=2), false);
    EXPECT_EQ(s5.at(x=3), false);
}

TEST(Expression, Misc2) {
    dimension x, y;
    std::vector<std::vector<int>> source = {{{1,-2},{-3,4},{0,6}}};
    dynamic_array a(x, y, source);
    dynamic_array b = (a > 0) * a;
    int sum = add(b, x, y);
    
    EXPECT_EQ(a.at(x=0, y=0), 1);
    EXPECT_EQ(a.at(x=1, y=0), -3);
    EXPECT_EQ(a.at(x=2, y=0), 0);
    EXPECT_EQ(a.at(x=0, y=1), -2);
    EXPECT_EQ(a.at(x=1, y=1), 4);
    EXPECT_EQ(a.at(x=2, y=1), 6);

    EXPECT_EQ(b.at(x=0, y=0), 1);
    EXPECT_EQ(b.at(x=1, y=0), 0);
    EXPECT_EQ(b.at(x=2, y=0), 0);
    EXPECT_EQ(b.at(x=0, y=1), 0);
    EXPECT_EQ(b.at(x=1, y=1), 4);
    EXPECT_EQ(b.at(x=2, y=1), 6);

    EXPECT_EQ(sum, 11);
}

TEST(Expression, Misc3) {
    dimension x, y;
    std::vector<std::array<int, 2>> source = {{{1,-2},{-3,4},{1,6}}};
    dynamic_array a(x, source, y);
    dynamic_array b = (a > 0) * a * 2;
    static_array c = add(b, x);
    int m = max(c, y);

    EXPECT_EQ(a.at(x=0, y=0), 1);
    EXPECT_EQ(a.at(x=1, y=0), -3);
    EXPECT_EQ(a.at(x=2, y=0), 1);
    EXPECT_EQ(a.at(x=0, y=1), -2);
    EXPECT_EQ(a.at(x=1, y=1), 4);
    EXPECT_EQ(a.at(x=2, y=1), 6);

    EXPECT_EQ(b.at(x=0, y=0), 2);
    EXPECT_EQ(b.at(x=1, y=0), 0);
    EXPECT_EQ(b.at(x=2, y=0), 2);
    EXPECT_EQ(b.at(x=0, y=1), 0);
    EXPECT_EQ(b.at(x=1, y=1), 8);
    EXPECT_EQ(b.at(x=2, y=1), 12);

    EXPECT_EQ(c.at(y=0), 4);
    EXPECT_EQ(c.at(y=1), 20);

    EXPECT_EQ(m, 20);
}

TEST(Expression, EliminatingDynamicDimension) {
    dimension x, y;
    dynamic_array a(x=3, y=7_c);
    static_array b = add((a > 0) * a + 2, x);
}

TEST(Expression, MatrixMultiplication) {
    dimension x, y, z;
    dynamic_array a(x=4_c, y=7);
    dynamic_array b(z=8_c, y=7);
    static_array c = add(a*b, y);
}

TEST(Expression, ExampleOfAutoUsage) {
    dimension x, y;
    dynamic_array a(x=4_c);
    static_array b(y=8_c);
    auto c_expression = a-b; // 1.
    static_array c = a-b; // 2.
}

TEST(Expression, ApplyOp) {
    dimension<int> x;

    std::array<int, 2> sourceA = {1, 2};
    std::array<int, 2> sourceB = {3, -4};

    static_array a(x, sourceA);
    static_array b(x, sourceB);

    static_array s1 = apply_op(std::plus<>(), a, b);
    EXPECT_EQ(s1.at(x=0), 4);
    EXPECT_EQ(s1.at(x=1), -2);
    static_array s2 = apply_op(std::multiplies<>(), a, b);
    EXPECT_EQ(s2.at(x=0), 3);
    EXPECT_EQ(s2.at(x=1), -8);
    static_array s3 = apply_op([](const auto &l, const auto &r) { return l*r*r; }, a, b);
    EXPECT_EQ(s3.at(x=0), 9);
    EXPECT_EQ(s3.at(x=1), 32);
}

}