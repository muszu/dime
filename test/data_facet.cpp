#include "gtest/gtest.h"
#include "dime/dynamic_array.hpp"
#include "dime/static_array.hpp"
#include "dime/extent.hpp"
#include "dime/types.hpp"

#include <iostream>
#include <array>
#include <vector>

namespace {

using namespace dime;

TEST(DataFacet, DynamicArrayWithArray) {
    dimension<int> x;

    std::array<int, 7> arr_7;

    dynamic_array da_x7C(x, arr_7);
    EXPECT_TRUE(
        (std::is_same<decltype(da_x7C.source), std::unique_ptr<std::vector<int>, std::default_delete<std::vector<int>>>>::value));
	EXPECT_EQ(da_x7C.source->size(), 7);
}

TEST(DataFacet, DynamicArrayWithVector) {
    dimension<int> x;

    std::vector<int> vec_4 = std::vector<int>(4);

    dynamic_array da_x4(x, vec_4);

    EXPECT_TRUE(
        (std::is_same<decltype(da_x4.source), std::unique_ptr<std::vector<int>, std::default_delete<std::vector<int>>>>::value));
	EXPECT_EQ(da_x4.source->size(), 4);
}

TEST(DataFacet, DynamicArrayWithNestedVector) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    std::vector<std::vector<int>> vec_5_3 = std::vector<std::vector<int>>(5, std::vector<int>(3));

    dynamic_array da_x5a3(z, vec_5_3);
    EXPECT_TRUE(
        (std::is_same<decltype(da_x5a3.source), std::unique_ptr<std::vector<int>, std::default_delete<std::vector<int>>>>::value));
	EXPECT_EQ(da_x5a3.source->size(), 5*3);

    dynamic_array da_x5y3(x, y, vec_5_3);
    EXPECT_TRUE(
        (std::is_same<decltype(da_x5y3.source), std::unique_ptr<std::vector<int>, std::default_delete<std::vector<int>>>>::value));
	EXPECT_EQ(da_x5y3.source->size(), 5*3);
}

TEST(DataFacet, DynamicArrayExtent) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dimension<short> w;

    dynamic_array da_x4y3z5Cw1(x=4, y=3, z=5_c, w=1);
    EXPECT_TRUE(
        (std::is_same<decltype(da_x4y3z5Cw1.source), std::unique_ptr<std::vector<int>, std::default_delete<std::vector<int>>>>::value));
	EXPECT_EQ(da_x4y3z5Cw1.source->size(), 4*3*5*1);
}


TEST(DataFacet, DynamicArrayExtentWithArbitrary) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dimension<short> w;

    dynamic_array da_x4yAz5CwA(x=4, ~y, z=5_c, ~w);
    EXPECT_TRUE(
        (std::is_same<decltype(da_x4yAz5CwA.source), std::unique_ptr<std::vector<int>, std::default_delete<std::vector<int>>>>::value));
	EXPECT_EQ(da_x4yAz5CwA.source->size(), 4*5);
}

TEST(DataFacet, StaticArrayWithArray) {
	dimension<int> x;

    std::array<int,4> arr_4 = {1,2,3,4};

    static_array sa_x4C(x, arr_4);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_x4C.source), std::array<int, 4>>::value));
}

TEST(DataFacet, StaticArrayWithNestedArray) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    std::array<std::array<std::array<int, 3>, 2>, 4> arr_4_2_3 = {};
    static_array sa_x4Cy2Cz3C(x, y, z, arr_4_2_3);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_x4Cy2Cz3C.source), std::array<int, 3*2*4>>::value));
}

TEST(DataFacet, StaticArrayExtent) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    static_array sa_x2C(x=2_c);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_x2C.source), std::array<int, 2>>::value));

    static_array sa_x2Cy3Cz5C(x=2_c, y=3_c, z=5_c);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_x2Cy3Cz5C.source), std::array<int, 2*3*5>>::value));
}

TEST(DataFacet, StaticArrayExtentWithArbitrary) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    static_array sa_xAy2C(~x, y=2_c);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_xAy2C.source), std::array<int, 2>>::value));

    static_array sa_x2CyAz5C(x=2_c, ~y, z=5_c);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_x2CyAz5C.source), std::array<int, 2*5>>::value));
}

TEST(DataFacet, DataType) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    static_array sa_bool(~x, y=2_c, element_type<bool>);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_bool.source), std::array<bool, 2>>::value));

    static_array sa_string(x=1_c, y=2_c, z=3_c, element_type<char>);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_string.source), std::array<char, 2*3>>::value));

    std::array<std::string, 2> arr = {"te", "st"};
    static_array sa_arr(arr);
    EXPECT_TRUE(
        (std::is_same<decltype(sa_arr.source), std::array<std::string, 2>>::value));

    dynamic_array da_bool(~x, y=2, element_type<bool>);
    EXPECT_TRUE(
        (std::is_same<decltype(da_bool.source), std::unique_ptr<std::vector<bool>, std::default_delete<std::vector<bool>>>>::value));

    dynamic_array da_string(x=1_c, y=2, z=3_c, element_type<char>);
    EXPECT_TRUE(
        (std::is_same<decltype(da_string.source), std::unique_ptr<std::vector<char>, std::default_delete<std::vector<char>>>>::value));

    std::vector<std::string> vec(4, "test");
    dynamic_array da_vec(vec);
    EXPECT_TRUE(
        (std::is_same<decltype(da_vec.source), std::unique_ptr<std::vector<std::string>, std::default_delete<std::vector<std::string>>>>::value));
}

TEST(DataFacet, CopyingData) {
	dimension<int> x;
    dimension y;

    std::array<int,3> arr_3 = {1,2,3};
    static_array s1(x, arr_3);
    EXPECT_EQ(s1.at(x=0), 1);
    EXPECT_EQ(s1.at(x=1), 2);
    EXPECT_EQ(s1.at(x=2), 3);

    std::array<std::array<int, 2>,3> arr_2_3 = {{{1,2},{3,4},{5,6}}};
    static_array s2(x, y, arr_2_3);
    EXPECT_EQ(s2.at(x=0, y=0), 1);
    EXPECT_EQ(s2.at(x=1, y=0), 3);
    EXPECT_EQ(s2.at(x=2, y=0), 5);
    EXPECT_EQ(s2.at(x=0, y=1), 2);
    EXPECT_EQ(s2.at(x=1, y=1), 4);
    EXPECT_EQ(s2.at(x=2, y=1), 6);
}

TEST(Expression, CalculatingOffsets) {
    dimension x, y;
    std::array<std::vector<int>, 3> source = {{{1,-2},{-3,4},{1,6}}};
    dynamic_array a(source, x, y);

    EXPECT_EQ(a.at(x=0, y=0), 1);
    EXPECT_EQ(a.at(x=1, y=0), -3);
    EXPECT_EQ(a.at(x=2, y=0), 1);
    EXPECT_EQ(a.at(x=0, y=1), -2);
    EXPECT_EQ(a.at(x=1, y=1), 4);
    EXPECT_EQ(a.at(x=2, y=1), 6);
}

}