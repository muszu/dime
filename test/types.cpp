#include "gtest/gtest.h"
#include "dime/constants.hpp"
#include "dime/types.hpp"

namespace {

using namespace dime::ypo;

struct s
{
	s(int);
	void operator()(int);
	int operator()(float) const;
};

TEST(Types, Equality) {
	static_assert(type<int> == type<int>);
	static_assert(type<int> != type<const int>);

	static_assert(type<decltype(value(type<int>))> == type<int>);
	static_assert(type<decltype(value(type<s>))> == type<s>);

	static_assert(invoke_result(type<s>, type<int>) == type<void>);
	static_assert(invoke_result(type<const s>, type<int>) == type<int>);

	static_assert(conditional<true>(type<int>, type<void>) == type<int>);
	static_assert(conditional(2_c == 3_c, type<int>, type<void>) == type<void>);
}

void test(const int x)
{
	static_assert(type_{x} == type<const int>);
	static_assert(type_{2} == type<int>);
}

std::true_type check(Type);
std::false_type check(...);

TEST(Types, TypeConcept) {
	static_assert(decltype(check(type<int>)){});
	static_assert(not decltype(check(0)){});
}

constexpr auto tint = type<int>;

TEST(Types, AccessingType) {
	static_assert(std::is_same_v<type_type<tint>, int>);
}

template <typename T>
struct g
{
	g(...);
};

template <typename T>
g(T,T) -> g<T>;

TEST(Types, Guide) {
	static_assert(guide<g>(type<int>, type<int>) == type<g<int>>);
}

template<typename T>
struct v {};
template<typename T>
struct u {};

TEST(Types, CheckingInstanceOfTemplate) {
	static_assert(is_instance_of_template<v>(type<v<int>>));
	static_assert(is_instance_of_template<v>(type<v<bool>>));
	static_assert(! is_instance_of_template<v>(type<u<bool>>));
	static_assert(! is_instance_of_template<v>(type<void>));
}

}
