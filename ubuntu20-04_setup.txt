# install gcc compiler for c++
    #sudo apt-get install gcc-9
    sudo apt-get install g++-9
    #sudo apt-get install g++
# clang
    sudo apt-get install clang-format clang-tidy clang-tools clang clangd libc++-dev libc++1 libc++abi-dev libc++abi1 libclang-dev libclang1 liblldb-dev libllvm-ocaml-dev libomp-dev libomp5 lld lldb llvm-dev llvm-runtime llvm python-clang
# clang 10 /usr/bin/clang++-10
    sudo apt-get install clang-10 lldb-10 lld-10
# tools
    sudo apt-get install cmake
    sudo apt-get install ninja-build
# set path for cmake
    export CXX=/usr/bin/g++-9
# clone repo
    git clone --recurse-submodules git@gitlab.com:muszu/dime.git