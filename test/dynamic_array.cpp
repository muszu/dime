#include "gtest/gtest.h"
#include "dime/dynamic_array.hpp"
#include "dime/static_array.hpp"
#include "dime/extent.hpp"
#include "dime/types.hpp"
#include "dime/arrays_utils.hpp"

#include <iostream>
#include <array>
#include <vector>

namespace {

using namespace dime;

TEST(DynamicArray, Constructor) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dynamic_array a(x=2_c);
    dynamic_array b(x=3);
    dynamic_array c(x=1_c, y=2, z=4);
    dynamic_array d(~z);
    std::vector<int> v = std::vector<int>(5);
    static_assert(std::is_same_v<decltype(dynamic_array(x,v)), decltype(dynamic_array(v, x))>);
    dynamic_array e(x, v);
    std::vector<std::vector<int>> u = std::vector<std::vector<int>>(5, std::vector<int>(3));
    dynamic_array f(u);
    dynamic_array g(x, u, y);
    dynamic_array h(z, u);
}

TEST(DynamicArray, CopyConstructor) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    std::vector<int> source = {{0,1,2,3}};
    std::array<int, 5> static_source = {{0,1,2,3}};

    dynamic_array a(x, source);
    EXPECT_EQ(a.at(x=0), 0);
    EXPECT_EQ(a.at(x=1), 1);
    EXPECT_EQ(a.at(x=2), 2);
    EXPECT_EQ(a.at(x=3), 3);

    dynamic_array b(a);
    EXPECT_EQ(b.at(x=0), 0);
    EXPECT_EQ(b.at(x=1), 1);
    EXPECT_EQ(b.at(x=2), 2);
    EXPECT_EQ(b.at(x=3), 3);

    static_array c(x, static_source);
    EXPECT_EQ(c.at(x=0), 0);
    EXPECT_EQ(c.at(x=1), 1);
    EXPECT_EQ(c.at(x=2), 2);
    EXPECT_EQ(c.at(x=3), 3);

    dynamic_array d(c);
    EXPECT_EQ(d.at(x=0), 0);
    EXPECT_EQ(d.at(x=1), 1);
    EXPECT_EQ(d.at(x=2), 2);
    EXPECT_EQ(d.at(x=3), 3);

    dynamic_array e(std::move(d));
    EXPECT_EQ(e.at(x=0), 0);
    EXPECT_EQ(e.at(x=1), 1);
    EXPECT_EQ(e.at(x=2), 2);
    EXPECT_EQ(e.at(x=3), 3);

    shared_array f(x, static_source);
    EXPECT_EQ(f.at(x=0), 0);
    EXPECT_EQ(f.at(x=1), 1);
    EXPECT_EQ(f.at(x=2), 2);
    EXPECT_EQ(f.at(x=3), 3);

    dynamic_array g(f);
    EXPECT_EQ(g.at(x=0), 0);
    EXPECT_EQ(g.at(x=1), 1);
    EXPECT_EQ(g.at(x=2), 2);
    EXPECT_EQ(g.at(x=3), 3);
}

TEST(DynamicArray, Const) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    std::array<const int, 5> static_source = {{0,1,2,3}};
    dynamic_array a(x, static_source);
    EXPECT_TRUE((std::is_const_v<typename decltype(a)::ElementType>));
    EXPECT_EQ(a.at(x=0), 0);
    EXPECT_EQ(a.at(x=1), 1);
    EXPECT_EQ(a.at(x=2), 2);
    EXPECT_EQ(a.at(x=3), 3);
    EXPECT_TRUE((std::is_const_v<std::remove_reference_t<decltype(a(x=2))>>));
}

TEST(DynamicArray, At) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dynamic_array a(x=2_c);
    a.at(x=0) = 2;
    a.at(x=1) = 3;
    EXPECT_EQ(a.at(x=0), 2);
    EXPECT_EQ(a.at(x=1), 3);
    EXPECT_EQ(a.at(x=1_c), 3);
    
    dynamic_array b(x=3);
    b.at(x=0) = 2;
    b.at(x=2_c) = 4;
    EXPECT_EQ(b.at(x=0), 2);
    EXPECT_EQ(b.at(x=2), 4);
    EXPECT_EQ(b.at(x=2_c), 4);
    
    dynamic_array c(x=1_c, y=2, z=4);
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 4; j++) {
            c(x=0, y=i, z=j) = i + j*2;
        }
    }
    bool match = true;
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 4; j++) {
            match = match && (c.at(x=0, y=i, z=j) == i + j*2);
        }
    }
    EXPECT_TRUE(match);
    
    dynamic_array d(~z);
    d.at(z=100) = 5;
    d.at(z=24_c) = 42;
    EXPECT_EQ(d.at(z=0), 42);
}

namespace test_dynamic_array_dim_concept
{
    dimension<int> x;
    dimension<bool> y;
    dimension<struct z> z;

    template<typename T>
        requires ArrayWithDims<decltype(x=4_c), T>
    bool f(T a)
    {
        return true;
    }
}

TEST(DynamicArray, DimConcept) {
    dimension<int> x;
    dimension<bool> y;
    dimension<struct z> z;
    dynamic_array a(x=4);
    dynamic_array b(y=4);
    dynamic_array c(x=4, y=5_c, ~z);
    dynamic_array d(x=4_c);
    
    EXPECT_TRUE((ArrayWithDims<decltype(x=4), decltype(a)>));
    EXPECT_TRUE((ArrayWith1Dim<decltype(x=4), decltype(a)>));
    EXPECT_FALSE((ArrayWith1Dim<decltype(y=4), decltype(a)>));
    EXPECT_TRUE((ArrayWith1Dim<decltype(x=42), decltype(a)>));
    EXPECT_FALSE((ArrayWith1Dim<decltype(y=5_c), decltype(a)>));
    EXPECT_FALSE((ArrayWith2Dims<decltype(x=4), decltype(y=5_c), decltype(a)>));

    EXPECT_TRUE(test_dynamic_array_dim_concept::f(d));
}

}