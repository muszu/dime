#pragma once

#include "types.hpp"
#include "dimensions.hpp"
#include "extent.hpp"
#include "dim_facet.hpp"
#include "strides_facet.hpp"
#include "element_type_facet.hpp"
#include "data_facet.hpp"
#include "static_array.hpp"
#include "dynamic_array.hpp"
#include "array_view.hpp"
#include "utils.hpp"

#include <tuple>
#include <type_traits>
#include <utility>

namespace dime
{
    template<typename T>
    struct get_mock_from_outer
    {
        static_assert(always_false_v<T>);
    };
    template<typename... T>
    struct get_mock_from_outer<std::tuple<T...>>
    {
        using type = std::tuple<const assigned_dim<typename T::DimensionType, constant_<0>> &...>;
    };

    template<typename Shape, typename F, typename... T>
    struct expression
    {
        using Outer = Shape;

        F f;
        std::tuple<const T & ...> t;
        expression(F f, std::tuple<const T & ...> t) : f(f), t(t) {};

        template<typename TU>
        constexpr auto inline filter_removed(TU&& t) const
        {
            if constexpr(std::tuple_size<TU>::value == std::tuple_size<Outer>::value)
            {
                return std::forward<decltype(t)>(t);
            }
            else
            {
                constexpr auto rs = range_sequence<std::tuple_size_v<TU>>.filter([](Constant c){
                    return constant<contains_equal_in<std::tuple_element_t<c.value, TU>, Outer>::value>; 
                });
                return sub_tuple(t, rs);
            }
        }

        template<typename D>
        auto dimSize(const D & d) const
        {
            return std::get<0>(filter<D>(outer())).size;
        }

        auto outer() const {
            if constexpr (sizeof...(T) == 0)
            {
                static_assert(always_false_v<T...>);
                return std::tuple<>();
            }
            else if constexpr (sizeof...(T) == 1)
            {
                return filter_removed(combine(std::tuple_cat(std::get<0>(t).outer())));
            }
            else
            {
                static_assert(sizeof...(T) == 2);
                
                using TypeL = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<0, decltype(t)>>>;
                using TypeR = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<1, decltype(t)>>>;

                if constexpr (ArrayLike<TypeL> && ArrayLike<TypeR>)
                {
                    return filter_removed(combine(std::tuple_cat(std::get<0>(t).outer(), std::get<1>(t).outer())));
                }
                else if constexpr (ArrayLike<TypeL> && !ArrayLike<TypeR>)
                {
                    return filter_removed(combine(std::get<0>(t).outer()));
                }
                else if constexpr (ArrayLike<TypeR> && !ArrayLike<TypeL>)
                {
                    return filter_removed(combine(std::get<1>(t).outer()));
                }
                else
                {   
                    static_assert(always_false_v<T...>);
                    return std::tuple<>();
                }
            }
        }

        template<typename... I>
        auto at(const std::tuple<const I & ...> i) const {
            if constexpr (sizeof...(T) == 0)
            {
                static_assert(always_false_v<T...>);
                return f(i);
            }
            else if constexpr (sizeof...(T) == 1)
            {
                return f(std::get<0>(t), i);
            }
            else
            {
                static_assert(sizeof...(T) == 2);
                return f(std::get<0>(t), std::get<1>(t), i);
            }
        }

        template<typename... I>
        auto at(const std::tuple<I...> i) const {
            if constexpr (sizeof...(T) == 0)
            {
                static_assert(always_false_v<T...>);
                return f(i);
            }
            else if constexpr (sizeof...(T) == 1)
            {
                return f(std::get<0>(t), i);
            }
            else
            {
                static_assert(sizeof...(T) == 2);
                return f(std::get<0>(t), std::get<1>(t), i);
            }
        }

        template<typename... I>
        auto at(I... i) const
        {
            return at(std::forward_as_tuple(i...));
        }

        using ResultElementType = decltype(std::declval<expression<Shape, F, T...>>().at(std::declval<typename get_mock_from_outer<Outer>::type>()));
        constexpr auto getElementType() const
        {
            return element_type<ResultElementType>;
        }
    };

    template<typename Shape, typename F, typename... T>
    constexpr auto create_expression(F f, std::tuple<const T & ...> t) {
        return expression<Shape, F, T...>(f, t);
    }

    namespace detail
    {
        constexpr inline auto at(const auto & arg, const auto &... indices)
        {
            using T = decltype(arg);
            if constexpr(ArrayLike<T>)
            {
                return arg.at(indices...);
            }
            else
            {
                return arg;
            }
        }
        template<typename T>
        struct get_outer {
            static_assert(always_false_v<T>);
        };
        template<typename T>
            requires ArrayLike<T>
        struct get_outer<T> {
            using type = typename std::remove_reference_t<T>::Outer;
        };
        template<typename T>
            requires !ArrayLike<T>
        struct get_outer<T> {
            using type = std::tuple<>;
        };
    }

    #define MAKE_BINARY(OP) \
    template<typename L, typename R> \
        requires ArrayLike<L> || ArrayLike<R> \
    auto operator OP(const L & lhs, const R & rhs) \
    { \
        return create_expression<typename CombineExtents<typename detail::tuple_cat_type<typename detail::get_outer<L>::type, typename detail::get_outer<R>::type>::type>::type>( \
            [](auto const & l, auto const & r, auto const & i) \
            { \
                return detail::at(l, i) OP detail::at(r, i); \
            }, \
            std::forward_as_tuple(lhs, rhs) \
        ); \
    } \

    MAKE_BINARY(+)
    MAKE_BINARY(-)
    MAKE_BINARY(*)
    MAKE_BINARY(/)
    MAKE_BINARY(%)
    MAKE_BINARY(&)
    MAKE_BINARY(|)
    MAKE_BINARY(^)
    MAKE_BINARY(<<)
    MAKE_BINARY(>>)
    MAKE_BINARY(==)
    MAKE_BINARY(!=)
    MAKE_BINARY(<)
    MAKE_BINARY(>)
    MAKE_BINARY(<=)
    MAKE_BINARY(>=)
    // MAKE_BINARY(<=>) // gcc: not yet implemented
    #undef MAKE_BINARY

    #define MAKE_PREFIX(OP) \
    template<typename L> \
        requires (Array<L> || ArrayView<L> || Expression<L>) \
    auto operator OP(const L & lhs) \
    { \
        return create_expression<typename L::Outer>( \
            [](auto const & l, auto const & i) \
            { \
                return OP l.at(i); \
            }, \
            std::forward_as_tuple(lhs) \
        ); \
    } \

    MAKE_PREFIX(~)
    MAKE_PREFIX(!)
    #undef MAKE_PREFIX

    template<typename Op, typename L, typename R>
        requires ArrayLike<L> || ArrayLike<R>
    auto apply_op(Op op, const L & lhs, const R & rhs)
    {
        return create_expression<typename CombineExtents<typename detail::tuple_cat_type<typename detail::get_outer<L>::type, typename detail::get_outer<R>::type>::type>::type>(
            [op](auto const & l, auto const & r, auto const & i)
            {
                return op(detail::at(l, i), detail::at(r, i));
            },
            std::forward_as_tuple(lhs, rhs)
        );
    }

    template<ArrayLike L, Dimension D>
    auto add(const L &l, const D &d)
    {
        if constexpr (std::tuple_size<typename L::Outer>::value == 1)
        {
            auto result = l.at(std::make_tuple(assigned_dim<D, int>(0)));
            size_t extent_size = l.dimSize(d).size;
            for(int i=1; i < extent_size; i++)
            {
                result += l.at(std::make_tuple(assigned_dim<D, int>(i)));
            }
            return result;
        }
        else
        {
            return create_expression<typename RemoveInnerType<std::tuple<D>, typename L::Outer>::type>(
                [](auto const & l, auto const & r, auto const & indexes)
                {   
                    auto result = l.at(std::tuple_cat(indexes, std::make_tuple(assigned_dim<D, int>(0))));
                    size_t extent_size = l.dimSize(r).size;
                    for(int i=1; i < extent_size; i++)
                    {
                        result += l.at(std::tuple_cat(indexes, std::make_tuple(assigned_dim<D, int>(i))));
                    }
                    return result;
                },
                std::forward_as_tuple(l, d));
        }
    }

    template<ArrayLike L, Dimension D1, Dimension D2, Dimension... Ds>
    auto add(const L &l, const D1 &d1, const D2 &d2, const Ds & ... ds)
    {
        return add(add(l, d2, ds...), d1);
    }

    template<typename Op, ArrayLike L, Dimension D>
    auto collapse(Op op, const L &l, const D &d)
    {
        if constexpr (std::tuple_size<typename L::Outer>::value == 1)
        {
            auto result = l.at(std::make_tuple(assigned_dim<D, int>(0)));
            size_t extent_size = l.dimSize(d).size;
            for(int i=1; i < extent_size; i++)
            {
                result = op(result, l.at(std::make_tuple(assigned_dim<D, int>(i))));
            }
            return result;
        }
        else
        {
            return create_expression<typename RemoveInnerType<std::tuple<D>, typename L::Outer>::type>(
                [op](auto const & l, auto const & r, auto const & indexes)
                {   
                    auto result = l.at(std::tuple_cat(indexes, std::make_tuple(assigned_dim<D, int>(0))));
                    size_t extent_size = l.dimSize(r).size;
                    for(int i=1; i < extent_size; i++)
                    {
                        result = op(result, l.at(std::tuple_cat(indexes, std::make_tuple(assigned_dim<D, int>(i)))));
                    }
                    return result;
                },
                std::forward_as_tuple(l, d));
        }
    }

    template<typename Op, ArrayLike L, Dimension D1, Dimension D2, Dimension... Ds>
    auto collapse(Op op, const L &l, const D1 &d1, const D2 &d2, const Ds & ... ds)
    {
        return collapse(op, collapse(op, l, d2, ds...), d1);
    }

    template<ArrayLike L, Dimension... Ds>
    auto mul(const L &l, const Ds & ... ds)
    {
        return collapse(std::multiplies<>(), l, ds...);
    }

    template<ArrayLike L, Dimension... Ds>
    auto min(const L &l, const Ds & ... ds)
    {
        return collapse([](const auto &lhs, const auto &rhs) { return std::min(lhs, rhs);}, l, ds...);
    }

    template<ArrayLike L, Dimension... Ds>
    auto max(const L &l, const Ds & ... ds)
    {
        return collapse([](const auto &lhs, const auto &rhs) { return std::max(lhs, rhs);}, l, ds...);
    }
}
