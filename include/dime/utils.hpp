#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "sequences.hpp"

using namespace dime::ypo;

namespace dime {

template <typename... T>
struct always_false : std::false_type {};

template<typename...T>
constexpr auto always_false_v = always_false<T...>::value;

enum Devices { CPU, GPU };

template <Devices D>
struct device_type_ {};

template <Devices D>
constexpr auto device_type = device_type_<D>{};

template <typename T>
concept DimensionName = true;

template <DimensionName... Ns>
struct dimension_list;
template <DimensionName N>
struct dimension;
template <DimensionName N>
struct anonymous_dimension;

namespace detail
{
       struct is_dimension_list
    {
        template <typename... T>
        constexpr auto operator()(dimension_list<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_dimension
    {
        template <typename... T>
        constexpr auto operator()(dimension<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_anonymous_dimension
    {
        template <typename... T>
        constexpr auto operator()(anonymous_dimension<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
}

template <typename T>
concept DimensionList = static_cast<bool>(value(invoke_result(type<detail::is_dimension_list>, type<T>)));
template <typename T>
concept Dimension = static_cast<bool>(value(invoke_result(type<detail::is_dimension>, type<T>)));
template <typename T>
concept AnonymousDimension = static_cast<bool>(value(invoke_result(type<detail::is_anonymous_dimension>, type<T>)));
template <typename T>
concept DimensionType = Dimension<T> || AnonymousDimension<T>;


template<DimensionType D, typename T, int ID = 0>
struct indexing;
template<typename T>
struct index_converter;

template <typename... Facets>
struct dynamic_array;
template <typename... Facets>
struct shared_array;
template <typename... Facets>
struct static_array;

template<typename... T>
struct array_view;

template<typename Shape, typename F, typename... T>
struct expression;

namespace detail
{
    struct is_device_type
    {
        template<Devices D>
        constexpr auto operator()(device_type_<D>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_expression
    {
        template<typename... T>
        constexpr auto operator()(expression<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_indexing
    {
        template<typename D, typename T, int S>
        constexpr auto operator()(const indexing<D, T, S> &)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_array_view
    {
        template <typename...T>
        constexpr auto operator()(array_view<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_static_array
    {
        template <typename...T>
        constexpr auto operator()(static_array<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_dynamic_array
    {
        template <typename...T>
        constexpr auto operator()(dynamic_array<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
    struct is_shared_array
    {
        template <typename...T>
        constexpr auto operator()(shared_array<T...>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
}

template<typename T>
concept DeviceType = static_cast<bool>(value(invoke_result(type<detail::is_device_type>, type<T>)));

template <typename T>
concept Indexing = static_cast<bool>(value(invoke_result(type<detail::is_indexing>, type<T>)));

template <typename T>
concept DynamicArray = static_cast<bool>(value(invoke_result(type<detail::is_dynamic_array>, type<T>)));
template <typename T>
concept SharedArray = static_cast<bool>(value(invoke_result(type<detail::is_shared_array>, type<T>)));
template <typename T>
concept StaticArray = static_cast<bool>(value(invoke_result(type<detail::is_static_array>, type<T>)));

template<typename T>
concept Array = DynamicArray<T> or SharedArray<T> or StaticArray<T>;
template <typename T>
concept ArrayView = static_cast<bool>(value(invoke_result(type<detail::is_array_view>, type<T>)));
template <typename T>
concept ViewSource = ArrayView<T> || Array<T>;
template<typename T>
concept Expression = static_cast<bool>(std::invoke_result_t<detail::is_expression, T>{});
template<typename T>
concept ArrayLike = Array<T> || ArrayView<T> || Expression<T>;

namespace detail
{
template<template <typename...> typename T>
struct is_instance_of_template{
    template<typename... U>
    constexpr auto operator()(T<U...>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};

constexpr auto array_like_predicate = [](Type t)
{
    return constant<ArrayLike<typename decltype(t)::type>>;
};

template<int I, typename T>
struct get {
    static_assert(always_false_v<T>);
};
template<typename T, typename... Ts>
struct get<0, std::tuple<T, Ts...>> {
    using type = T;
};
template<int I, typename T, typename... Ts>
struct get<I, std::tuple<T, Ts...>> {
    using type = typename get<I-1, std::tuple<Ts...>>::type;
};
}

template <typename D, typename Value>
struct assigned_dim;

template<int S, typename L, typename R, typename E, typename... I>
constexpr void loop(L & l, const R & r, const E & e, const I & ... indexes)
{
    if constexpr (S == std::tuple_size<E>::value) {
        l.at(indexes...) = r.at(indexes...);
    } else {
        const auto limit = std::get<S>(e).size.size;
        for(int i=0; i < limit; i++)
        {
            loop<S+1>(l, r, e, indexes..., assigned_dim<typename std::tuple_element_t<S, E>::DimensionType, int>(i));
        }
        if (limit == 0)
        {
            throw std::runtime_error("extent with size 0 in loop");
        }
    }
}

template<int S, typename L, typename R, typename E, typename... I>
constexpr void copy_loop(L & l, const R & r, const E & e, size_t offset, size_t stride, const I & ... indexes)
{
    if constexpr (S == std::tuple_size<E>::value) {
        l.at(offset) = r.at(indexes...);
    } else {
        const auto limit = std::get<S>(e).size.size;
        for(int i=0; i < limit; i++)
        {
            copy_loop<S+1>(l, r, e, i*stride + offset, stride*limit, indexes..., assigned_dim<typename std::tuple_element_t<S, E>::DimensionType, int>(i));
        }
        if (limit == 0)
        {
            throw std::runtime_error("extent with size 0 in loop");
        }
    }
}

inline constexpr auto&& wrap_initializer_list(auto && val)
{
    return std::forward<decltype(val)>(val);
}
template<typename T, size_t S1>
inline constexpr auto wrap_initializer_list(T (&&value)[S1])
{
    std::array<T, S1> result{};
    for (size_t i = 0; i < S1; ++i)
    {
        result[i] = std::forward<T>(value[i]);
    }
    return result;
}
template<typename T, size_t S1, size_t S2>
inline constexpr auto wrap_initializer_list(T (&&value)[S1][S2])
{
    std::array<std::array<T, S2>, S1> result{};
    for (size_t i = 0; i < S1; ++i)
    {
        for (size_t j = 0; j < S2; ++j)
        {
            result[i][j] = std::forward<T>(value[i][j]);
        }
    }
    return result;
}
template<typename T, size_t S1, size_t S2, size_t S3>
inline constexpr auto wrap_initializer_list(T (&&value)[S1][S2][S3])
{
    std::array<std::array<T, S2>, S1> result{};
    for (size_t i = 0; i < S1; ++i)
    {
        for (size_t j = 0; j < S2; ++j)
        {
            for (size_t k = 0; k < S3; ++k)
            {
                result[i][j][k] = std::forward<T>(value[i][j][k]);
            }
        }
    }
    return result;
}
template<typename T, size_t S1, size_t S2, size_t S3, size_t S4>
inline constexpr auto wrap_initializer_list(T (&&value)[S1][S2][S3][S4])
{
    std::array<std::array<T, S2>, S1> result{};
    for (size_t i = 0; i < S1; ++i)
    {
        for (size_t j = 0; j < S2; ++j)
        {
            for (size_t k = 0; k < S3; ++k)
            {
                for (size_t l = 0; l < S4; ++l)
                {
                    result[i][j][k][l] = std::forward<T>(value[i][j][k][l]);
                }
            }
        }
    }
    return result;
}

template<typename... T, std::size_t... Is>
constexpr auto sub_tuple(const std::tuple<T...> &t, sequence_<Is...>)
{
    return std::make_tuple(std::get<Is>(t)...);
}
template<typename... T, std::size_t... Is>
constexpr auto forward_sub_tuple(const std::tuple<T...> &t, sequence_<Is...>)
{
    return std::forward_as_tuple(std::get<Is>(t)...);
}

template<typename D, typename TU>
constexpr auto inline filter(TU t)
{
    constexpr auto rs = range_sequence<std::tuple_size_v<TU>>.filter([](Constant c){
        return constant<std::is_same_v<typename std::tuple_element_t<c.value, TU>::DimensionType, D>>; 
    });
    return sub_tuple(t, rs);
}

}
