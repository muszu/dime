#pragma once

#include "constants.hpp"
#include "types.hpp"
#include "dimensions.hpp"

#include <type_traits>
#include <utility>
#include <array>
#include <vector>
#include <tuple>

using namespace dime::ypo;

namespace dime {

namespace detail
{
    struct is_vector_or_array
    {
        template<typename T>
        constexpr auto operator()(std::vector<T>)
        {
            return constant<true>;
        }
        template<typename T, std::size_t S>
        constexpr auto operator()(std::array<T,S>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
}

template<typename T>
concept VectorOrArray = static_cast<bool>(std::invoke_result_t<dime::detail::is_vector_or_array, T>{});

template<typename T, DimensionName N = decltype([]{})>
auto getSTLConatinerShape(const std::vector<T> &vector);

template<typename T, std::size_t S, DimensionName N = decltype([]{})>
constexpr auto getSTLConatinerShape(const std::array<T,S> &array)
{
    using Size = static_size<S>;
    if constexpr (std::invoke_result_t<dime::detail::is_vector_or_array, T>{})
    {
        return std::tuple_cat(
            std::make_tuple(extent(anonymous_dimension<N>(), Size{})),
            getSTLConatinerShape(array[0]));
    }
    else
    {
        return  std::make_tuple(extent(anonymous_dimension<N>(), Size{}));
    }
}

template<typename T, DimensionName N = decltype([]{})>
auto getSTLConatinerShape(const std::vector<T> &vector)
{
    if constexpr (std::invoke_result_t<dime::detail::is_vector_or_array, T>{})
    {
        return std::tuple_cat(
            std::make_tuple(extent(anonymous_dimension<N>(), dynamic_size(vector.size()))),
            getSTLConatinerShape(vector[0]));
    }
    else
    {
        return std::tuple(extent(anonymous_dimension<N>(), dynamic_size(vector.size())));
    }
}

template<typename T>
struct getSTLConatinerDataType {
    using type = T;
};
template<typename T>
struct getSTLConatinerDataType<std::vector<T>> {
    using type = getSTLConatinerDataType<T>::type;
};
template<typename T, std::size_t S>
struct getSTLConatinerDataType<std::array<T, S>> {
    using type = getSTLConatinerDataType<T>::type;
};

template<typename D>
struct data {
    D value;
    constexpr data(const D &data) : value(data) {};
    constexpr data(D &&data) : value(std::forward<D>(data)) {};
};

namespace detail
{
struct is_data
{
    template <typename T>
    constexpr auto operator()(data<T>)
    {
        return constant<true>;
    }
    constexpr auto operator()(...)
    {
        return constant<false>;
    }
};
}

constexpr auto data_predicate = [](Type t)
{
    return value(invoke_result(type<detail::is_data>, t));
};

template<typename S>
struct shape {
    S value;
    constexpr shape(const S &shape) : value(shape) {};
    constexpr shape(S &&shape) : value(std::forward<S>(shape)) {};
};

namespace detail
{
    struct is_shape
    {
        template<typename T>
        constexpr auto operator()(shape<T>)
        {
            return constant<true>;
        }
        constexpr auto operator()(...)
        {
            return constant<false>;
        }
    };
}

constexpr auto shape_predicate = [](Type t)
{
    return value(invoke_result(type<detail::is_shape>, t));
};

}
