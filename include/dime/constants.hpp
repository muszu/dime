#pragma once

#include <cstdint>     // uintmax_t
#include <type_traits> // conditional, invoke_result, is_same
#include <utility>     // forward

namespace dime::ypo {


// an improvement of std::integral_constant with lifted operators

template <auto V>
struct constant_
{
	using type = decltype(V);

	static constexpr type value = V;

	constexpr operator type() const
	{
		return V;
	}
};

#define MAKE_PREFIX(OP) \
template <auto V> \
constexpr constant_<(OP V)> operator OP(constant_<V>) \
{ \
	return {}; \
}

MAKE_PREFIX(+)
MAKE_PREFIX(-)
MAKE_PREFIX(~)
MAKE_PREFIX(!)
MAKE_PREFIX(*)
MAKE_PREFIX(++)
MAKE_PREFIX(--)
#undef MAKE_PREFIX

#define MAKE_POSTFIX(OP) \
template <auto V> \
constexpr constant_<(V OP)> operator OP(constant_<V>, int) \
{ \
	return {}; \
}

MAKE_POSTFIX(++)
MAKE_POSTFIX(--)
#undef MAKE_POSTFIX

#define MAKE_BINARY(OP) \
template <auto V1, auto V2> \
constexpr constant_<(V1 OP V2)> operator OP(constant_<V1>, constant_<V2>) \
{ \
	return {}; \
} \
template <typename T1, auto V2> \
constexpr auto operator OP(T1 && v1, constant_<V2>) \
{ \
	return std::forward<T1>(v1) OP V2; \
} \
template <auto V1, typename T2> \
constexpr auto operator OP(constant_<V1>, T2 && v2) \
{ \
	return V1 OP std::forward<T2>(v2); \
}

MAKE_BINARY(+)
MAKE_BINARY(-)
MAKE_BINARY(*)
MAKE_BINARY(/)
MAKE_BINARY(%)
MAKE_BINARY(&)
MAKE_BINARY(|)
MAKE_BINARY(^)
// MAKE_BINARY(<<) // not compatible with googletest
// MAKE_BINARY(>>) // not compatible with googletest
MAKE_BINARY(==)
MAKE_BINARY(!=)
MAKE_BINARY(<)
MAKE_BINARY(>)
MAKE_BINARY(<=)
MAKE_BINARY(>=)
// MAKE_BINARY(<=>) // gcc: not yet implemented
#undef MAKE_BINARY


// a concept satisfied by constant types

namespace detail
{
	template <typename T = void>
	struct is_constant
	{
		template <auto V>
		requires (std::is_same_v<T, decltype(V)> or std::is_same_v<T, void>)
		constexpr auto operator ()(constant_<V>) const
		{
			return constant_<true>{};
		}

		constexpr auto operator ()(...) const
		{
			return constant_<false>{};
		}
	};
}

template <typename C, typename T = void>
concept Constant = static_cast<bool>(std::invoke_result_t<detail::is_constant<T>, C>());


// short syntax for creating constants

template <auto V>
constexpr auto constant = std::conditional_t<Constant<decltype(V)>, decltype(V), constant_<V>>{};


// convenient syntax for writing integer constants

namespace detail
{
	constexpr std::uintmax_t accumulate_positional(std::uintmax_t, std::uintmax_t accumulated)
	{
		return accumulated;
	};

	constexpr std::uintmax_t accumulate_positional(std::uintmax_t base, std::uintmax_t accumulated, std::uintmax_t digit, auto... digits)
	{
		return accumulate_positional(base, base * accumulated + digit, digits...);
	};

	constexpr std::uintmax_t char_to_digit(char c)
	{
		switch (c)
		{
			case '0': return 0;
			case '1': return 1;
			case '2': return 2;
			case '3': return 3;
			case '4': return 4;
			case '5': return 5;
			case '6': return 6;
			case '7': return 7;
			case '8': return 8;
			case '9': return 9;
			case 'A': case 'a': return 10;
			case 'B': case 'b': return 11;
			case 'C': case 'c': return 12;
			case 'D': case 'd': return 13;
			case 'E': case 'e': return 14;
			case 'F': case 'f': return 15;
			default: return -1;
		}
	}
}

template <char... Cs>
struct positional_constant_ : constant_<detail::accumulate_positional(10UL, 0UL, detail::char_to_digit(Cs)...)>
{
};

template <char... Cs>
struct positional_constant_<'0', Cs...> : constant_<detail::accumulate_positional(8UL, 0UL, detail::char_to_digit(Cs)...)>
{
};

template <char... Cs>
struct positional_constant_<'0', 'b', Cs...> : constant_<detail::accumulate_positional(2UL, 0UL, detail::char_to_digit(Cs)...)>
{
};

template <char... Cs>
struct positional_constant_<'0', 'x', Cs...> : constant_<detail::accumulate_positional(16UL, 0UL, detail::char_to_digit(Cs)...)>
{
};

template <char... Cs>
constexpr auto positional_constant = constant_(positional_constant_<Cs...>{});

template <char... Cs>
constexpr auto operator ""_c()
{
	return positional_constant<Cs...>;
}



}
