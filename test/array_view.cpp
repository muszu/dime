#include "gtest/gtest.h"
#include "dime/array_view.hpp"
#include "dime/static_array.hpp"
#include "dime/dynamic_array.hpp"
#include "dime/extent.hpp"
#include "dime/types.hpp"

namespace {

using namespace dime;

TEST(ArrayView, BasicConstruction) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dimension<struct w> w;
    
    dynamic_array a(x=2_c, y=3_c, z=5_c, w=3);
    array_view blank_replace = array_view(a, x=1, y=w, z=2);
    EXPECT_TRUE((std::is_same_v<
        decltype(blank_replace.outer()),
        std::tuple<dime::extent<decltype(w), dime::static_size<3> > >
    >));
    EXPECT_TRUE((std::is_same_v<
        decltype(blank_replace)::Outer,
        std::tuple<dime::extent<decltype(w), dime::static_size<3> > >
    >));
    EXPECT_TRUE((std::is_same_v<
        decltype(blank_replace.dimSize(w)),
        static_size<3>
    >));

    dynamic_array b(x=2_c, y=3_c);
    array_view bv = array_view(b, x=y, y=z);
    EXPECT_TRUE((std::is_same_v<
        decltype(bv)::Outer,
        std::tuple<dime::extent<decltype(y), dime::static_size<2> >, dime::extent<decltype(z), dime::static_size<3> > >
    >));

    dynamic_array c(x=2_c, y=2_c);
    array_view cv = array_view(c, indexing<decltype(x), decltype(y), 0>(), indexing<decltype(y), decltype(z), 1>());
    EXPECT_TRUE((std::is_same_v<
        decltype(cv)::Outer,
        std::tuple<dime::extent<decltype(z), dime::static_size<2> > >
    >));

    array_view bcv = array_view(b, indexing<decltype(y), decltype(c), 0>(c));
    EXPECT_TRUE((std::is_same_v<
        decltype(bcv)::Outer,
        std::tuple<dime::extent<decltype(x), dime::static_size<2> >, dime::extent<decltype(y), dime::static_size<2> > >
    >));

    array_view bcvv = array_view(bcv, x=1, y=x);
    EXPECT_TRUE((std::is_same_v<
        decltype(bcvv)::Outer,
        std::tuple<dime::extent<decltype(x), dime::static_size<2> > >
    >));
}

TEST(ArrayView, ParenthesesOperator) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dimension<struct w> w;
    
    dynamic_array a(x=2_c, y=2, z=5_c);
    array_view v1 = array_view(a, x=y);
    EXPECT_TRUE((std::is_same_v<
        decltype(v1)::Outer,
        std::tuple<dime::extent<dime::dimension<bool>, dime::static_size<2> >, dime::extent<dime::dimension<char>, dime::static_size<5> > >
    >));

    array_view v2 = v1(z=2);
    EXPECT_TRUE((std::is_same_v<
        decltype(v2)::Outer,
        std::tuple<dime::extent<dime::dimension<bool>, dime::static_size<2> > >
    >));

    array_view v3 = v1(y=1);
    EXPECT_TRUE((std::is_same_v<
        decltype(v3)::Outer,
        std::tuple<dime::extent<dime::dimension<char>, dime::static_size<5> > >
    >));

    int element = v3(z=3);    
}

TEST(ArrayView, IndexingWithArraySimple) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    
    dynamic_array a(x=2_c, y=4, z=5_c);
    dynamic_array b(x=2_c, y=3_c);
    dynamic_array c(x=2_c, y=4_c);
    array_view v1 = array_view(a);
    std::array<int, 2> i2;
    array_view v2 = v1(z=i2);
    EXPECT_TRUE((std::is_same_v<
        decltype(v2)::Outer,
        std::tuple<dime::extent<dime::dimension<int>, dime::static_size<2> >, dime::extent<dime::dimension<bool>, dime::dynamic_size>,
            dime::extent<dime::dimension<char>, dime::static_size<2> > >
    >));

    try
    {
        array_view v3 = v1(z=b);
        FAIL() << "Expected dime::incompatible_sizes_exception";
    }
    catch(incompatible_sizes_exception const & err)
    {
        EXPECT_EQ(err.what(), std::string("Combine called with incompatible sizes."));
    }
    catch(...)
    {
        FAIL() << "Expected dime::incompatible_sizes_exception";
    }

    array_view v4 = v1(z=c);
    EXPECT_TRUE((std::is_same_v<
        decltype(v4)::Outer,
        std::tuple<dime::extent<dime::dimension<int>, dime::static_size<2> >, dime::extent<dime::dimension<bool>, dime::static_size<4> > >
    >));
}

TEST(ArrayView, AccessingData) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    std::array<int, 4> ar = {4,3,2,1};
    std::array<std::array<int, 2>, 2> a2 = {{{4,3},{2,1}}};
    static_array a(x, ar);
    array_view v1 = array_view(a);
    EXPECT_EQ(a.at(x=0), 4);
    EXPECT_EQ(v1.at(x=0), 4);

    std::array<std::array<int, 2>, 3> br = {{{6,5}, {4,3},{2,1}}};
    static_array b(x, y, br); // x=3, y=2
    array_view v2 = array_view(b, y=0);
    EXPECT_EQ(v2.at(x=1_c), 4);

    array_view v3 = array_view(b, y=z);
    EXPECT_EQ(v3.at(x=1, z=1_c), 3);

    array_view v4 = array_view(b, y=ar);
    EXPECT_EQ(v4.at(x=1, y=3_c), 3);

    std::vector<int> ccr = {{2,1,0,1}};
    std::array<int, 4> cr = {2,1,0,1};
    array_view v5 = array_view(b, x=ccr);
    EXPECT_EQ(v5.at(x=0, y=0), 2);
    EXPECT_EQ(v5(x=1, y=0), 4);
    EXPECT_EQ(v5.at(x=2, y=0), 6);
    EXPECT_EQ(v5.at(x=3, y=0), 4);
}

TEST(ArrayView, NestedView) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    static_array a(x=3_c, y=2_c, z=4_c);
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 2; j++) {
            for(int k = 0; k < 4; k++) {
                a(x=i, y=j, z=k) = (i+1)*100+(j+1)*10+k+1;
            }   
        }    
    }
    array_view v1 = array_view(a, x=y, y=x, z=2);
    EXPECT_EQ(v1.at(x=0, y=1), 213);
    EXPECT_EQ(v1.at(x=1, y=2), 323);

    array_view v2 = v1(y=x, x=y);
    EXPECT_EQ(v2.at(x=0, y=1), 123);
    EXPECT_EQ(v2.at(x=1, y=0), 213);
    EXPECT_EQ(v2.at(x=1, y=1), 223);
    EXPECT_EQ(v2.at(x=2, y=1), 323);

    std::vector<size_t> vec2 = {0, 1, 0};
    array_view v3 = v2(y=vec2);
    EXPECT_EQ(v3.at(x=0, y=0), 113);
    EXPECT_EQ(v3.at(x=1, y=1), 223);
    EXPECT_EQ(v3(x=2, y=2), 313);
    EXPECT_EQ(v3.at(x=2, y=0), 313);
    EXPECT_EQ(v3.at(x=0, y=2), 113);

    array_view v4 = v3(y=x);
    EXPECT_EQ(v4.at(x=0), 113);
    EXPECT_EQ(v4(x=1), 223);
    EXPECT_EQ(v4.at(x=2), 313);
}

TEST(ArrayView, NestedViewWithArbitrary) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dimension arbit1, arbit2;
    static_array a(x=3_c, ~arbit1, y=2_c, ~arbit2, z=4_c);
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 2; j++) {
            for(int k = 0; k < 4; k++) {
                a.at(x=i, arbit1=42, y=j, arbit2=i*j*k, z=k) = (i+1)*100+(j+1)*10+k+1;
            }   
        }    
    }
    array_view v1 = array_view(a, x=y, y=x, z=2, arbit1=321);
    EXPECT_EQ(v1.at(x=0, arbit2=4214535, y=1), 213);
    EXPECT_EQ(v1.at(x=1, y=2, arbit2=42), 323);

    array_view v2 = v1(y=x, x=y, arbit2=y);
    EXPECT_EQ(v2.at(x=0, y=1), 123);
    EXPECT_EQ(v2.at(x=1, y=0), 213);
    EXPECT_EQ(v2.at(x=1, y=1), 223);
    EXPECT_EQ(v2.at(x=2, y=1), 323);

    array_view v3 = v2(y={0,1,0});
    EXPECT_EQ(v3(x=0, y=0), 113);
    EXPECT_EQ(v3.at(x=1, y=1), 223);
    EXPECT_EQ(v3.at(x=2, y=2), 313);
    EXPECT_EQ(v3.at(x=2, y=0), 313);
    EXPECT_EQ(v3.at(x=0, y=2), 113);

    array_view v4 = v3(y=x);
    EXPECT_EQ(v4.at(x=0), 113);
    EXPECT_EQ(v4.at(x=1), 223);
    EXPECT_EQ(v4(x=2), 313);
}

TEST(ArrayView, ReplacingDimensions) {
    dimension<int> x;
    dimension<bool> y;
    std::array<int, 4> ar = {4,3,2,1};
    static_array a(x, ar);
    static_array a2 = a(x=y);
    EXPECT_EQ(a2(y=1), 3);

    dynamic_array b(x, ar);
    dynamic_array b2 = b(x=y);
    EXPECT_EQ(b2(y=2), 2);

    shared_array c(x, ar);
    shared_array c2 = c(x=y);
    EXPECT_EQ(c2(y=3), 1);
}
}