#pragma once

#include "constants.hpp"
#include "features.hpp"
#include "types.hpp"
#include "utils.hpp"
#include "dimensions.hpp"
#include "extent.hpp"

#include <tuple>
#include <type_traits>

namespace dime
{

template <Extent... Ns>
struct dim_facet;

constexpr auto dim_feature_predicate = [](Type t)
{
    return std::invoke_result_t<dime::detail::is_dimension_type, typename decltype(t)::type>{}
        | std::invoke_result_t<dime::detail::is_instance_of_template<shape>, typename decltype(t)::type>{}
        | std::invoke_result_t<dime::detail::is_instance_of_template<extent>, typename decltype(t)::type>{}
        | std::invoke_result_t<dime::detail::is_instance_of_template<dim_facet>, typename decltype(t)::type>{};
};

namespace detail
{
template<typename... T>
struct anonymous_dimension_number {
    static constexpr auto value = constant<-1>;
};
template<Extent E, Extent F, typename... T>
struct anonymous_dimension_number<E, std::tuple<F, T...>> {
    static constexpr auto value = constant<1> + anonymous_dimension_number<E, std::tuple<T...>>::value;
};
template<Extent E, typename... T>
struct anonymous_dimension_number<E, std::tuple<E, T...>> {
    static constexpr auto value = constant<0>;
};

template<typename... T>
struct find_extent {
    using type = void;
};
template<typename DN, Extent E, Extent... T>
struct find_extent<DN, E, T...> {
    using type = std::conditional_t<
        std::is_same_v<DN, typename E::DimensionType::dimension_name>,
        E,
        typename find_extent<DN, T...>::type
    >;
};
}

template<Extent E, typename T>
constexpr auto getExtendArgs(Features const & fs)
{
    using D = typename E::DimensionType;
    using S = typename E::SizeType;

    if constexpr(StaticOrArbitrarySize<S>)
    {
        return std::tuple<>();
    }
    else if constexpr(DynamicSize<S>)
    {
        // 1. try finding extent with dimension D
        auto ext = fs(extend_with_name_predicate<D>);
        static_assert(std::tuple_size_v<decltype(ext)> <= 1, "This tuple should not have more than one element.");

        if constexpr (std::tuple_size_v<decltype(ext)> == 1)
        {
            return std::get<0>(ext).size;
        }
        // 2. if failed then find size of anonymous dimension
        else
        {
            auto tpl_sh = fs(shape_predicate);
            static_assert(std::tuple_size_v<decltype(tpl_sh)> == 1);
            const auto & sh = std::get<0>(tpl_sh).value;
            constexpr int index = detail::anonymous_dimension_number<E, T>::value;
            static_assert(index != -1, "Matching anonymous dimension wasn't found");
            return dynamic_size(std::get<index>(sh).size.size);
        }
    }
    else
    {
        static_assert(always_false_v<E>, "dim_facet should contains only well defined extents, blank is not accepted");
        return std::tuple<>();
    }
}

template <Extent... Ns>
struct dim_facet : Ns...
{
    using extents = std::tuple<Ns...>;
    using dims = std::tuple<Ns...>;
    using dims_ = std::tuple<typename Ns::DimensionType...>;
    using dims_names = std::tuple<typename Ns::DimensionType::dimension_name...>;

    using Outer = std::tuple<Ns...>;
    constexpr std::tuple<const Ns & ...> outer() const {
        return std::forward_as_tuple(static_cast<const Ns &>(*this)...);
    }

    constexpr dim_facet(Features const & fs) : Ns(getExtendArgs<Ns, std::tuple<Ns...>>(fs))...
    {
    }
    constexpr dim_facet(dim_facet<Ns...> const & d) : Ns(static_cast<Ns const &>(d))...
    {
    }

    std::string dimString() const
    {
        std::string result = "Dimensions:\n";
        ((result += (std::string(ypo::type_name<Ns>()) + ",\n")), ...);
        result.erase(result.end() - 2);
        return result;
    }

    template<typename T>
    auto constexpr dimSize() const
    {
        return detail::find_extent<T, Ns...>::type::size;
    }
    template<DimensionType D>
    auto constexpr dimSize(const D &) const
    {
        return detail::find_extent<typename D::dimension_name, Ns...>::type::size;
    }
};

template<typename T>
concept DimFacet = static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<dim_facet>, T>{});

namespace detail
{
    template<typename T>
    struct extract_extents_from_tuple_with_shape {
        static_assert(always_false_v<T>);
    };
    template<typename... T>
    struct extract_extents_from_tuple_with_shape<std::tuple<dime::shape<std::tuple<T...>>>> {
        using type = std::tuple<T...>;
    };

    template<template <typename...> typename T, typename... U>
    struct template_type_from_tuple {
        static_assert(always_false_v<T>);
    };
    template<template <typename...> typename T, typename... U>
    struct template_type_from_tuple<T, std::tuple<U...>> {
        using type = T<U...>;
    };

    template<typename... T>
    struct name_first_anonymous
    {
        static_assert(always_false_v<T...>, "Wrong usage of name_first_anonymous");
    };
    template<DimensionType D, Extent... B, typename N, typename S, Extent... Es>
    struct name_first_anonymous<D, std::tuple<B...>, std::tuple<extent<anonymous_dimension<N>, S>, Es...>>
    {
        using type = std::tuple<B..., extent<D, S>, Es...>;
    };
    template<DimensionType D, Extent... B, Extent E, Extent... Es>
    struct name_first_anonymous<D, std::tuple<B...>, std::tuple<E, Es...>>
    {
        using type = typename name_first_anonymous<D, std::tuple<B..., E>, std::tuple<Es...>>::type;
    };


    template<typename... T>
    struct rename_extents
    {
        static_assert(always_false_v<T...>);
    };
    template<Extent... B>
    struct rename_extents<std::tuple<>, std::tuple<B...>>
    {   
        using type = std::tuple<B...>;
    };
    template<DimensionType A, DimensionType... As, Extent... B>
    struct rename_extents<std::tuple<A, As...>, std::tuple<B...>>
    {   
        using type = typename rename_extents<
            std::tuple<As...>,
            typename name_first_anonymous<A, std::tuple<>, std::tuple<B...>>::type
        >::type;
    };

    template<typename... T>
    struct tuple_cat_type {
        static_assert(always_false_v<T...>);
    };
    template<typename... T>
    struct tuple_cat_type<std::tuple<T...>> {
        using type = std::tuple<T...>;
    };
    template<typename... T, typename... V, typename... R>
    struct tuple_cat_type<std::tuple<T...>, std::tuple<V...>, R...> {
        using type = typename tuple_cat_type<std::tuple<T..., V...>, R...>::type;
    };
}

template<typename... T>
struct dim_features_to_dim_facet
{
    static_assert(always_false_v<T...>);
};

template<typename... T>
    requires ((static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<shape>, T>{}) ? 1 : 0 ) + ...) == 1 &&
        (((static_cast<bool>(std::invoke_result_t<dime::detail::is_dimension, T>{}) ? 1 : 0 ) + ...) == (sizeof...(T) - 1))
struct dim_features_to_dim_facet<std::tuple<T...>>
{
    using dimensions = typename detail::tuple_cat_type<std::conditional_t<
            static_cast<bool>(std::invoke_result_t<dime::detail::is_dimension, T>{}),
            std::tuple<std::remove_reference_t<T>>,
            std::tuple<>>...>::type;

    using shape_extents = typename detail::extract_extents_from_tuple_with_shape<typename detail::tuple_cat_type<std::conditional_t<
        static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<shape>, T>{}),
        std::tuple<std::remove_reference_t<T>>,
        std::tuple<>>...>::type>::type;

    static_assert(std::tuple_size<dimensions>::value <= std::tuple_size<shape_extents>::value, "More dimensions provided than shape of data contains.");

    // Use dims to rename anonymous dimensions.
    using shape_extents_with_named = typename detail::rename_extents<dimensions, shape_extents>::type;

    using type = typename detail::template_type_from_tuple<dim_facet, shape_extents_with_named>::type;
};

template<typename... T>
    requires (static_cast<bool>(std::invoke_result_t<dime::detail::is_instance_of_template<extent>, T>{}) and ...)
struct dim_features_to_dim_facet<std::tuple<T...>>
{
    using type = dim_facet<std::remove_reference_t<T>...>;
};

template<DimFacet T>
struct dim_features_to_dim_facet<std::tuple<T>>
{
    using type = std::remove_cv_t<std::remove_reference_t<T>>;
};

template <Features F>
using dim_facet_from = typename dim_features_to_dim_facet<std::invoke_result_t<F, decltype(dim_feature_predicate)>>::type;

}