#pragma once

#include "constants.hpp"
#include "types.hpp"

#include <type_traits>

namespace dime::ypo {


template <auto... Vs>
struct sequence_;

template <auto... Vs>
constexpr auto sequence = sequence_<Vs...>{};


template <auto... Vs>
struct sequence_
{
	friend constexpr auto operator ==(sequence_<Vs...>, sequence_<Vs...>)
	{
		return constant<true>;
	}

	template <auto... Ws>
	friend constexpr auto operator ==(sequence_<Vs...>, sequence_<Ws...>)
	{
		return constant<false>;
	}

	template <auto... Ws>
	friend constexpr auto operator !=(sequence_<Vs...> vs, sequence_<Ws...> ws)
	{
		return not (vs == ws);
	}

	template <auto... Ws>
	friend constexpr auto operator +(sequence_<Vs...>, sequence_<Ws...>)
	{
		return sequence<Vs..., Ws...>;
	}

	constexpr auto contains(Constant w) const
	{
		return constant<((w == constant<Vs>) or ...)>;
	}

	constexpr auto filter(const auto & predicate) const
	{
		return (value(conditional(predicate(constant<Vs>), type<sequence_<Vs>>, type<sequence_<>>)) + ...);
	}
};


namespace detail
{
	struct is_sequence
	{
		template <auto... Vs>
		constexpr auto operator ()(sequence_<Vs...>) const
		{
			return constant<true>;
		}

		constexpr auto operator ()(...) const
		{
			return constant<false>;
		}
	};
}

template <typename T>
concept Sequence = static_cast<bool>(value(invoke_result(type<detail::is_sequence>, type<T>)));


namespace detail
{
	constexpr auto range_sequence(Constant n)
	{
		if constexpr (n.value)
			return range_sequence(n - constant<1>) + sequence<n.value - 1>;
		else
			return sequence<>;
	}
}

template <auto N>
constexpr auto range_sequence = detail::range_sequence(constant<N>);

template <auto N>
using range_sequence_ = std::remove_cv_t<decltype(range_sequence<N>)>;


}
