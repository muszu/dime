#pragma once

#include "constants.hpp"
#include "sequences.hpp"
#include "types.hpp"

#include <tuple>
#include <type_traits>
#include <utility>

namespace dime::ypo {


template <typename Factory, typename Tuple>
struct features : Tuple
{
	constexpr features(Factory factory, auto && ... initializers) :
		Tuple(std::tuple_cat(std::tuple(factory(initializers))...))
	{
	}

	template <std::size_t... Is>
	constexpr auto operator ()(sequence_<Is...>)
	{
		return std::forward_as_tuple(std::get<Is>(*this)...);
	}

	template <std::size_t... Is>
	constexpr auto operator ()(sequence_<Is...>) const
	{
		return std::forward_as_tuple(std::get<Is>(*this)...);
	}

	template <typename Predicate>
	constexpr auto operator ()(Predicate)
	{
		return operator()(range_sequence<std::tuple_size_v<Tuple>>.filter([](Constant c){ return Predicate()(type<std::tuple_element_t<c.value, Tuple>>); }));
	}

	template <typename Predicate>
	constexpr auto operator ()(Predicate) const
	{
		return operator()(range_sequence<std::tuple_size_v<Tuple>>.filter([](Constant c){ return Predicate()(type<std::tuple_element_t<c.value, Tuple>>); }));
	}

	constexpr std::size_t size()
	{
		return std::tuple_size_v<Tuple>;
	}
};

template <typename Factory, typename... Initializers>
features(Factory, Initializers && ...) -> features<Factory, decltype(std::tuple_cat(std::tuple(Factory()(std::declval<Initializers &&>()))...))>;

template <typename Factory, typename... Initializers>
using features_from = decltype(features(Factory{}, std::declval<Initializers>()...));


namespace detail
{
	struct is_features
	{
		template <typename... Ts>
		constexpr auto operator ()(const features<Ts...> &) const
		{
			return constant<true>;
		}

		constexpr auto operator ()(...) const
		{
			return constant<false>;
		}
	};
}

template <typename T>
concept Features = static_cast<bool>(value(invoke_result(type<detail::is_features>, type<T>)));


template <typename Factory, typename... Facets>
struct form : Facets...
{
	constexpr explicit form(auto && ... initializers) :
		form(features(Factory(), std::forward<decltype(initializers)>(initializers)...))
	{
	}

	constexpr form(Features && features) :
		Facets(features)...
	{
	}
};


template <typename T, typename Factory, auto ... Initializers>
concept Form = (static_cast<bool>(Factory()(Initializers)(type<T>)) and ...);



}
