#pragma once

#include "utils.hpp"
#include "types.hpp"
#include "constants.hpp"
#include "dimensions.hpp"
#include "dynamic_array.hpp"
#include "static_array.hpp"

#include <tuple>
#include <type_traits>

namespace dime
{
template<typename... T>
struct array_view
{
    static_assert(always_false_v<T...>);
};

template<DimensionType D, typename T, int ID>
struct indexing
{
    static constexpr int id = ID;
    static_assert(always_false_v<T>);
};

template<DimensionType D, int ID>
struct indexing<D, dynamic_size, ID>
{
    static constexpr int id = ID;
    size_t value;

    using Outer = std::tuple<>;
    using Inner = std::tuple<D>;

    auto outer() const
    {
        return std::tuple<>();
    }

    indexing(const std::size_t & value) : value(value) {}
    indexing(const dynamic_size & value) : value(value.size) {}
    template<typename T>
    indexing(const dime::assigned_dim<D, T>& a) : value(a.value) {}
    template<int ID2>
    indexing(const indexing<D, dynamic_size, ID2> & i) : value(i.value) {}
    indexing(const indexing<D, dynamic_size, ID> & i) : value(i.value) {}
};

template<DimensionType D, std::size_t size, int ID>
struct indexing<D, static_size<size>, ID>
{
    static constexpr int id = ID;
    static constexpr constant_<size> value{};

    using Outer = std::tuple<>;
    using Inner = std::tuple<D>;

    auto outer() const
    {
        return std::tuple<>();
    }

    constexpr indexing(const constant_<size> &) {}
    constexpr indexing(const static_size<size> &) {}
    constexpr indexing(const dime::assigned_dim<D, dime::ypo::constant_<size>> &) {}
    template<int ID2>
    constexpr indexing(const indexing<D, static_size<size>, ID2> &) {}
    constexpr indexing(const indexing<D, static_size<size>, ID> &) {}
    constexpr indexing() {}
};

template<DimensionType D, typename T, int ID>
struct indexing<D, std::vector<T>, ID>
{
    static constexpr int id = ID;
    std::vector<T> value;

    using Outer = std::tuple<extent<D, dynamic_size>>;
    using Inner = std::tuple<D>;

    auto outer() const
    {
        return std::make_tuple(extent<D, dynamic_size>(value.size()));
    }

    template<typename Z>
    constexpr auto at(const std::tuple<Z> & a) const
    {
        static_assert(std::is_same_v<typename D::Name, typename std::remove_reference_t<std::remove_cv_t<Z>>::dimension_type::Name >,
            "indexing - wrong argument for at");
        return dime::assigned_dim<D, int>(value.at(std::get<0>(a).value));
    }

    indexing(const dime::assigned_dim<D, std::vector<T>> & ad) : value(ad.value) {}
    constexpr indexing(const std::vector<T> & value) : value(value) {}
    template<int ID2>
    constexpr indexing(const indexing<D, std::vector<T>, ID2> & i) : value(i.value) {}
    constexpr indexing(const indexing<D, std::vector<T>, ID> & i) : value(i.value) {}
};

template<DimensionType D, typename T, std::size_t S, int ID>
struct indexing<D, std::array<T, S>, ID>
{
    static constexpr int id = ID;
    std::array<T, S> value;

    using Outer = std::tuple<extent<D, static_size<S>>>;
    using Inner = std::tuple<D>;

    constexpr auto outer() const
    {
        return std::make_tuple(extent<D, static_size<S>>());
    }

    template<typename Z>
    constexpr auto at(const std::tuple<Z> & a) const
    {
        static_assert(std::is_same_v<typename D::Name, typename std::remove_reference_t<std::remove_cv_t<Z>>::dimension_type::Name >,
            "indexing - wrong argument for at");

        return dime::assigned_dim<D, int>(value.at(std::get<0>(a).value));
    }

    indexing(const dime::assigned_dim<D, std::array<T, S>> & ad) : value(ad.value) {}
    template<int ID2>
    constexpr indexing(const indexing<D, std::array<T, S>, ID2> & i) : value(i.value) {}
    constexpr indexing(const indexing<D, std::array<T, S>, ID> & i) : value(i.value) {}
    constexpr indexing(const std::array<T, S> & value) : value(value) {}
    constexpr indexing(T (&&value)[S])
    {
        for (size_t i = 0; i < S; ++i)
        {
            this->value[i] = value[i];
        }
    }
};

template<DimensionType D, Array AL, int ID>
struct indexing<D, AL, ID>
{
    static constexpr int id = ID;
    const AL & value;

    using Outer = typename AL::Outer;
    using Inner = std::tuple<D>;

    auto outer() const
    {
        return value.outer();
    }

    template<typename... Z>
    constexpr auto at(const std::tuple<Z...> & a) const
    {
        return dime::assigned_dim<D, int>(value.at(a));
    }

    indexing(const dime::assigned_dim<D, AL> & ad) : value(ad.value) {}
    template<int ID2>
    indexing(const indexing<D, AL, ID2> & i) : value(i.value) {}
    indexing(const indexing<D, AL, ID> & i) : value(i.value) {}
    indexing(const AL & value) : value(value) {}
};

template<DimensionType D, Expression AL, int ID>
struct indexing<D, AL, ID>
{
    static constexpr int id = ID;
    const AL value;

    using Outer = typename AL::Outer;
    using Inner = std::tuple<D>;

    auto outer() const
    {
        return value.outer();
    }

    template<typename... Z>
    constexpr auto at(const std::tuple<Z...> & a) const
    {
        return dime::assigned_dim<D, int>(value.at(a));
    }

    indexing(const dime::assigned_dim<D, AL> & ad) : value(ad.value) {}
    template<int ID2>
    indexing(const indexing<D, AL, ID2> & i) : value(i.value) {}
    indexing(const indexing<D, AL, ID> & i) : value(i.value) {}
    indexing(const AL & value) : value(value) {}
};


template<DimensionType X, DimensionType Y, int ID>
struct indexing<X, Y, ID>
{
    static constexpr int id = ID;
    using Outer = std::tuple<extent<Y, blank_size>>;
    using Inner = std::tuple<X>;

    auto outer() const
    {
        return std::make_tuple(extent<Y, blank_size>());
    }

    template<typename Z>
    constexpr auto at(const std::tuple<Z> & a) const
    {
        static_assert(std::is_same_v<typename Y::Name, typename std::remove_reference_t<std::remove_cv_t<Z>>::dimension_type::Name >,
            "indexing - wrong argument for at");
        return dime::assigned_dim<X, int>(std::get<0>(a).value);
    }

    indexing() {}
    indexing(const dime::assigned_dim<X, Y> &) {}
    template<int ID2>
    indexing(const indexing<X, Y, ID2> &) {}
    indexing(const indexing<X, Y, ID> &) {}
};


template<typename T>
struct index_converter {
    static_assert(always_false_v<T>);
};
template<Indexing I>
struct index_converter<I> {
    using type = I;
};
template<AssignedDim U>
struct index_converter<U> {
    using type = indexing<std::remove_cv_t<decltype(U::dimension)>, std::remove_cv_t<typename U::value_type>>;
};
template<typename U>
struct index_converter<assigned_dim<U, int>> {
    using type = indexing<U, dynamic_size>;
};
template<typename U>
struct index_converter<assigned_dim<U, std::size_t>> {
    using type = indexing<U, dynamic_size>;
};
template<typename U, std::size_t S>
struct index_converter<assigned_dim<U, dime::ypo::constant_<S>>> {
    using type = indexing<U, static_size<S>>;
};
template<typename U, int S>
struct index_converter<assigned_dim<U, dime::ypo::constant_<S>>> {
    using type = indexing<U, static_size<S>>;
};

template<typename... T>
struct RemoveInnerType
{
    static_assert(always_false_v<T...>);
};
template<typename InnerExtent, typename... T>
struct RemoveInnerType<std::tuple<InnerExtent>, std::tuple<T...>>
{
    using type = detail::tuple_cat_type<
        std::conditional_t<std::is_same_v<typename InnerExtent::dimension_name, typename T::Name>, std::tuple<>, std::tuple<T>>...
    >::type;
};


template<typename Source, typename... Indexes>
constexpr auto getOuter(const array_view<Source, Indexes...> & a) {
    return outerAux<0>(a, a.source.outer());
}

template<int I, typename In, typename T>
auto removeInner(const T & t)
{
    static_assert(std::tuple_size<In>::value == 1);
    using InnerDimName = typename std::tuple_element_t<0, In>::dimension_name;

    if constexpr(std::tuple_size<T>::value == I)
    {
        return std::tuple<>();
    }
    else
    {   
        using ExtentAtI = std::remove_reference_t<std::tuple_element_t<I, T>>;
        if constexpr(std::is_same_v<typename ExtentAtI::Name, InnerDimName>)
        {
            return removeInner<I+1, In>(t);
        }
        else
        {
            return std::tuple_cat(std::make_tuple(std::get<I>(t)), removeInner<I+1, In>(t));
        }
    }
}

template<typename T, typename Index>
constexpr auto applyOuter(const T & t, const Index & i)
{
    return combine(std::tuple_cat(t, i.outer()));
}

template<int I, typename Source, typename... Indexes, typename T>
auto outerAux(const array_view<Source, Indexes...> & a, const T & t)
{
    if constexpr(sizeof...(Indexes) == I) {
        return t;
    }
    else
    {
        using IndexI = std::tuple_element_t<I, std::tuple<Indexes...>>;
        auto withoutInner = removeInner<0, typename IndexI::Inner>(t);
        const IndexI & indexI = static_cast<const IndexI &>(a);
        auto withOuter = applyOuter(withoutInner, indexI);
        return outerAux<I+1>(a, withOuter);
    }
}

template<typename... T>
struct is_in_conflict
{
    static_assert(always_false_v<T...>);
};
template<Indexing T>
struct is_in_conflict<T>
{
    static const bool value = false;
};
template<Indexing T, Indexing U, Indexing... Us>
struct is_in_conflict<T, U, Us...>
{
    static const bool value = std::is_same_v<typename T::Inner, typename U::Inner>
        || is_in_conflict<T, Us...>::value;
};
template<typename... T>
struct contains_conflicting_indexing
{
    static_assert(always_false_v<T...>);
};
template<Indexing... T>
struct contains_conflicting_indexing<std::tuple<T...>>
{
    static const bool value = false;
};
template<Indexing... T, Indexing U, Indexing... Us>
struct contains_conflicting_indexing<std::tuple<T...>, U, Us...>
{
    static const bool value = is_in_conflict<U, T...>::value || contains_conflicting_indexing<std::tuple<T...>, Us...>::value;
};
template<size_t I>
struct index_id {
    static const size_t ID = I;
};
template<typename... T>
struct get_index_id
{
    static_assert(always_false_v<T...>);
};
template<Array T>
struct get_index_id<T>
{
    static constexpr int value = 0;
    using type = index_id<value>;
};
template<ArrayView T>
struct get_index_id<T>
{
    static constexpr int value = 1 + get_index_id<typename T::source_type>::value;
    using type = index_id<value>;
};

template<typename... T>
struct max_indexing_level 
{
    static_assert(always_false_v<T...>);
};
template<>
struct max_indexing_level<>
{
    static const size_t value = 0;
};
template<Indexing I>
struct max_indexing_level<I>
{
    static const size_t value = I::id;
};
template<Indexing I, Indexing... Is>
struct max_indexing_level<I, Is...>
{
    static const size_t value = I::id > max_indexing_level<Is...>::value ? I::id : max_indexing_level<Is...>::value;
};
template<typename... T>
struct min_indexing_level 
{
    static_assert(always_false_v<T...>);
};
template<Indexing I>
struct min_indexing_level<I>
{
    static const size_t value = I::id;
};
template<Indexing I1, Indexing I2>
struct min_indexing_level<I1, I2>
{
    static const size_t value = I1::id > I2::id ? I2::id : I1::id;
};
template<Indexing I, Indexing... Is>
struct min_indexing_level<I, Is...>
{
    static const size_t value = I::id > min_indexing_level<Is...>::value ? min_indexing_level<Is...>::value : I::id;
};
template<size_t S, Indexing... I>
struct filter_indexing_by_level
{
    using type = detail::tuple_cat_type<
        std::conditional_t<S == I::id, std::tuple<I>, std::tuple<>>...
    >::type;
};

namespace detail
{
template<size_t BS, typename BE, typename... E>
constexpr auto replace_blank_in_extent_aux(const dime::ypo::sequence_<BS> &, const BE & be, const std::tuple<E...> & e)
{
    using DimensionType = typename std::remove_reference_t<std::remove_cv_t<std::tuple_element_t<0, typename BE::Outer>>>::DimensionType;
    auto element = std::get<BS>(e);
    return extent<DimensionType, typename std::remove_reference_t<std::remove_cv_t<decltype(element)>>::SizeType>(element.size);
}
template<typename BE, typename... E>
constexpr auto replace_blank_in_extent(const BE & be, const std::tuple<E...> & e)
{
    using InnerName = typename std::remove_reference_t<std::remove_cv_t<std::tuple_element_t<0, typename BE::Inner>>>::Name;
    constexpr auto positionOfExtendsWithMatchingName = range_sequence<std::tuple_size_v<std::tuple<E...>>>
        .filter([](Constant c){
            using ExtentName = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<c.value, std::tuple<E...>>>>::Name;
            return constant<std::is_same_v<ExtentName, InnerName>>;
        });
    return replace_blank_in_extent_aux(positionOfExtendsWithMatchingName, be, e);
}
template<size_t... BS, typename... B, typename... E>
constexpr auto replace_blanks_aux(const dime::ypo::sequence_<BS...> &, const std::tuple<B...> & b,  const std::tuple<E...> & e)
{
    return std::make_tuple(replace_blank_in_extent(std::get<BS>(b), e)...);
}
template<typename... B, typename... E>
constexpr auto replace_blanks(const std::tuple<B...> & b, const std::tuple<E...> & e)
{
    return replace_blanks_aux(range_sequence<std::tuple_size_v<std::tuple<B...>>>, b, e);
}
template<typename T>
struct IsExtentInTupleWithBlankSize {
    static_assert(std::tuple_size_v<T> == 1);
    static constexpr bool value = std::is_same_v< blank_size, typename std::tuple_element_t<0, T>::SizeType >;
};
template<typename... T>
struct GetInnerDimsOfIndexes {
    static_assert(always_false_v<T...>);
};
template<size_t... S, typename T>
struct GetInnerDimsOfIndexes<dime::ypo::sequence_<S...>, T> {
    using type = decltype(std::tuple_cat(std::declval<typename std::remove_reference_t<std::remove_cv_t<std::tuple_element_t<S, T>>>::Inner>()...));
};
template<typename... T>
struct ContainsDimensionWithName
{
    static_assert(always_false_v<T...>);
};
template<typename Name>
struct ContainsDimensionWithName<Name, std::tuple<>>
{
    static constexpr bool value = false;
};
template<typename Name, typename D, typename... Ds>
struct ContainsDimensionWithName<Name, std::tuple<D, Ds...>>
{
    static constexpr bool value = std::is_same_v<Name, typename std::remove_reference_t<std::remove_cv_t<D>>::dimension_name>
        || (std::is_same_v<Name, typename std::remove_reference_t<std::remove_cv_t<Ds>>::dimension_name> || ...);
};
template<typename I>
constexpr auto access_id(const I &) { return constant<I::id>; }
template<size_t... BS, typename... I>
auto get_extents_from_indexs(const dime::ypo::sequence_<BS...> &, const std::tuple<I...> & i)
{
    if constexpr (sizeof...(I) == 0)
    {
        return std::tuple<>();
    }
    else
    {
        return std::tuple_cat(std::get<BS>(i).outer()...);
    }
}
template<size_t S, typename... E, typename... I> // Extents, Indexes
auto outer_aux(const std::tuple<E...> & te, const std::tuple<I...> & ti) {
    if constexpr( S > max_indexing_level<std::remove_cv_t<std::remove_reference_t<I>>...>::value)
    {
        return te;
    }
    else
    {
        constexpr auto currentIndexesPosition = range_sequence<std::tuple_size_v<std::tuple<I...>>>
            .filter([](Constant c){
                return constant<S == std::tuple_element_t<c.value, std::tuple<std::remove_cv_t<std::remove_reference_t<I>>...> >::id>;
            });
        using InnerDimsOfIndexes = typename GetInnerDimsOfIndexes<std::remove_cv_t<decltype(currentIndexesPosition)>, std::tuple<I...>>::type;

        constexpr auto positionOfExtendsToKeep = range_sequence<std::tuple_size_v<std::tuple<E...>>>
            .filter([](Constant c){
                using ElementType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<c.value, std::tuple<E...>>>>::Name;
                constexpr bool isDimNameInInner = ContainsDimensionWithName<ElementType, InnerDimsOfIndexes>::value;
                return constant<not isDimNameInInner>;
            });

        auto te_inner_removed = forward_sub_tuple(te, positionOfExtendsToKeep);

        auto ti_current = forward_sub_tuple(ti, currentIndexesPosition);

        // Due to compiler bug get firstly those indexes that contains excatly one extent in Outer (no if constexpr in lambda)
        constexpr auto posOfExtentsWithOuterOfSizeOne = range_sequence<std::tuple_size_v<decltype(ti_current)>>
            .filter([](Constant c){
                using ElementType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<c.value, decltype(ti_current)>>>;
                using ElementOuter = typename ElementType::Outer;
                return constant<std::tuple_size_v<ElementOuter> == 1>;
            });
        auto ti_current_outer_size_one = forward_sub_tuple(ti_current, posOfExtentsWithOuterOfSizeOne);
        if constexpr(std::tuple_size_v<decltype(ti_current_outer_size_one)> != 0)
        {
            // Assuming indexing can contain blank size <=> it contains one extent in Outer <=> X=Y
            constexpr auto positionOfExtendsWithBlankSize = range_sequence<std::tuple_size_v<decltype(ti_current_outer_size_one)>>
                .filter([](Constant c){
                    using ElementType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<c.value, decltype(ti_current_outer_size_one)>>>;
                    using OuterExtentType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<0, typename ElementType::Outer>>>;
                    return constant<std::is_same_v< blank_size, typename OuterExtentType::SizeType>>;
                });
            auto outer_extents_with_blank = forward_sub_tuple(ti_current_outer_size_one, positionOfExtendsWithBlankSize);
            auto outer_extents_with_blank_replaced = replace_blanks(outer_extents_with_blank, te);

            constexpr auto posOfExtentsWithOuterOfSizeNotOne = range_sequence<std::tuple_size_v<decltype(ti_current)>>
            .filter([](Constant c){
                using ElementType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<c.value, decltype(ti_current)>>>;
                using ElementOuter = typename ElementType::Outer;
                return constant<std::tuple_size_v<ElementOuter> != 1>;
            });
            auto ti_current_outer_size_not_one = forward_sub_tuple(ti_current, posOfExtentsWithOuterOfSizeNotOne);
            constexpr auto positionOfExtendsWithNotBlankSize = range_sequence<std::tuple_size_v<decltype(ti_current_outer_size_one)>>
                .filter([](Constant c){
                    using ElementType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<c.value, decltype(ti_current_outer_size_one)>>>;
                    using OuterExtentType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<0, typename ElementType::Outer>>>;
                    return constant<not std::is_same_v< blank_size, typename OuterExtentType::SizeType>>;
                });
            auto outer_extents_with_not_blank = forward_sub_tuple(ti_current_outer_size_one, positionOfExtendsWithNotBlankSize);
            return outer_aux<S+1>(
                combine(std::tuple_cat(
                    te_inner_removed,
                    outer_extents_with_blank_replaced,
                    get_extents_from_indexs(range_sequence<std::tuple_size_v<decltype(ti_current_outer_size_not_one)>>, ti_current_outer_size_not_one),
                    get_extents_from_indexs(range_sequence<std::tuple_size_v<decltype(outer_extents_with_not_blank)>>, outer_extents_with_not_blank))),
                ti);
        }
        else
        {
            // No blank dimension, apply combine normally
            return outer_aux<S+1>(
                combine(std::tuple_cat(
                    te_inner_removed,
                    get_extents_from_indexs(range_sequence<std::tuple_size_v<decltype(ti_current)>>, ti_current))),
                ti);
        }
    }
}

template<typename... E, typename... I> // Extents, Indexes
auto outer(const std::tuple<E...> & te, const std::tuple<I...> & ti) {
    if constexpr(sizeof...(I) == 0)
    {
        return te;
    }
    else
    {
        return outer_aux<0>(te, ti);
    }
}

struct outer_callable
{
    template<typename E, typename I>
    auto operator () (const E & e, const I & i)
    {
        return outer(e, i);
    }
};
template<int NewId, typename T1, typename T2, int OldId>
constexpr auto change_index_id(const indexing<T1, T2, OldId> i)
{
    return indexing<T1, T2, NewId>(i);
}

template<size_t I, size_t ... Is, typename TupleOfIndexes>
constexpr auto find_min_indexing(const dime::ypo::sequence_<I, Is...>, const TupleOfIndexes & t)
{
    using FirstType = std::remove_cv_t<std::remove_reference_t< std::tuple_element_t<I, TupleOfIndexes> >>;
    constexpr size_t minid = min_indexing_level< FirstType,
        std::remove_cv_t<std::remove_reference_t< std::tuple_element_t<Is, TupleOfIndexes> >>... >::value;

    static_assert(FirstType::id == minid, "find_min_indexing min index was not leftmost");

    return I;
};
template<typename T, int S>
struct find_indexing_val_for_source_aux
{
    static_assert(always_false_v<T>);
};
template<int ID, typename E, typename TupleOfIndexes>
constexpr auto find_indexing_value_aux(const TupleOfIndexes & t)
{
    // Find all indexes with E::Name == Inner::Name and ID not less than current ID.
    constexpr auto indexPos = range_sequence<std::tuple_size_v<TupleOfIndexes>>
        .filter([](Constant c){
            using IndexType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<c.value, TupleOfIndexes>>>;
            using InnerType =  std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<0, typename IndexType::Inner>>>;
            return constant<IndexType::id >= ID && std::is_same_v<typename InnerType::Name, typename E::Name>>;
        });
    
    static_assert(not std::is_same_v<dime::ypo::sequence_<>, std::remove_cv_t<std::remove_reference_t<decltype(indexPos)>>>, "Not all dimensions were indexed.");
    // Potentialy find minimum, but the way array_view is constructed we have guarantee that leftmost index is the smallest one.
    constexpr int positionOfIndex = find_min_indexing(indexPos, t);
    using IndexType = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<positionOfIndex, TupleOfIndexes>>>;
    using IndexOuter = typename IndexType::Outer;
    if constexpr (std::tuple_size_v<IndexOuter> == 0)
    {
        return std::make_tuple(
            assigned_dim<typename E::DimensionType, int>(std::get<positionOfIndex>(t).value)
        );
    }
    else
    {
        auto nested = find_indexing_val_for_source_aux<IndexOuter, IndexType::id + 1>::find_indexing_values(t);
        auto currentIndex = std::get<positionOfIndex>(t);
        return std::make_tuple(assigned_dim<typename E::DimensionType, int>(currentIndex.at(nested)));
    }
}
template<typename... T, int S>
struct find_indexing_val_for_source_aux<std::tuple<T...>, S>
{
    template<typename TupleOfIndexes>
    static auto constexpr find_indexing_values(const TupleOfIndexes & t)
    {
        return std::tuple_cat(find_indexing_value_aux<S, std::remove_cv_t<std::remove_reference_t<T>>>(t)...);
    }
};
}

// Asssuming no gap between Indexes id!
template<ViewSource Source, Indexing... Indexes>
struct array_view<Source, Indexes...> : Indexes...
{
    using source_type = Source;
    using source_outer_type = decltype(std::declval<Source>().outer());
    using Outer = std::invoke_result_t<dime::detail::outer_callable, source_outer_type, std::tuple<Indexes...>>;
    using ElementType = typename Source::ElementType;
    Source & source;
    typename array_view<Source, Indexes...>::Outer outer_calculated = this->outer();

    array_view(Source & source, const Indexes &... indexes) : source(source), Indexes(indexes)... {}
    array_view(const array_view<Source, Indexes...> & av) : source(av.source), Indexes(static_cast<const Indexes &>(av))... {}

    constexpr auto outer() const {
        return detail::outer(source.outer(), std::forward_as_tuple(static_cast<const Indexes &>(*this)...));
    }
    template<typename... I2>
    constexpr auto operator_parentheses_implementation(const I2 & ... t)
    {
        static_assert(sizeof...(I2) != 0, "No indexes provided.");

        if constexpr (sizeof...(Indexes) == 0)
        {
            using NewOuter = typename array_view<Source, I2...>::Outer;
            if constexpr(std::tuple_size_v<NewOuter> == 0)
            {
                return this->at(t...);
            }
            else
            {
                return array_view<Source, I2...>(source, t...);
            }
        }
        else
        {
            using InnerOfNewIndexes = detail::tuple_cat_type<typename I2::Inner...>::type;
            using OuterOfOldIndexes = detail::tuple_cat_type<typename Indexes::Outer...>::type;

            constexpr auto checkIfUseDifferenId = range_sequence<std::tuple_size_v<OuterOfOldIndexes>>
                .filter([](Constant c){
                    using OuterName = std::remove_cv_t<std::remove_reference_t<std::tuple_element_t<c.value, OuterOfOldIndexes>>>::Name;
                    constexpr bool isConflictingName = dime::detail::ContainsDimensionWithName<OuterName, InnerOfNewIndexes>::value;
                    return constant<isConflictingName>;
                });

            if constexpr (not std::is_same_v<std::remove_cv_t<std::remove_reference_t<decltype(checkIfUseDifferenId)>>, dime::ypo::sequence_<>>)
            {
                constexpr int new_id = max_indexing_level<std::remove_cv_t<std::remove_reference_t<Indexes>>...>::value + 1;
                using NewOuter = typename array_view<Source, Indexes..., std::remove_cv_t<std::remove_reference_t<decltype(dime::detail::change_index_id<new_id>(t))>>...>::Outer;
                if constexpr(std::tuple_size_v<NewOuter> == 0)
                {
                    return this->at(t...);
                }
                else
                {
                    return array_view<Source, Indexes..., std::remove_cv_t<std::remove_reference_t<decltype(dime::detail::change_index_id<new_id>(t))>>...>(source, static_cast<const Indexes &>(*this)..., dime::detail::change_index_id<new_id>(t)...);
                }
            }
            else
            {
                using NewOuter = typename array_view<Source, Indexes..., I2...>::Outer;
                if constexpr(std::tuple_size_v<NewOuter> == 0)
                {
                    return this->at(t...);
                }
                else
                {
                    return array_view<Source, Indexes..., I2...>(source, static_cast<const Indexes &>(*this)..., t...);
                }
            }
        }
    }
    template<typename... I2>
    constexpr auto operator() (const I2 & ... i2) {
        return operator_parentheses_implementation((typename index_converter<I2>::type)(i2)...);
    }

    template<typename D>
    auto dimSize(const D & d) const
    {
        return std::get<0>(filter<D>(outer())).size;
    }

    template<typename... I>
    auto& at(I... i) const
    {
        static_assert(sizeof...(I) == std::tuple_size_v<Outer>, "at should be called with all dimensions");

        if constexpr (sizeof...(Indexes) == 0)
        {
            return source.at(i...);
        }
        else
        {
            constexpr int new_id = max_indexing_level<std::remove_cv_t<std::remove_reference_t<Indexes>>...>::value + 1;
            auto source_indexing = dime::detail::find_indexing_val_for_source_aux<source_outer_type, 0>::find_indexing_values(
                std::forward_as_tuple(static_cast<const Indexes &>(*this)...,
                dime::detail::change_index_id<new_id>(indexing(i))...
            ));
            return source.at(source_indexing);
        }
    }
    template<typename... I>
    auto& at(const std::tuple<I ...> i) const {
        return std::apply(at, i);
    }
};

template<typename D>
indexing(const dime::assigned_dim<D, int> &) -> indexing<D, dynamic_size, 0>;
template<DimensionType D1, int S>
indexing(const dime::assigned_dim<D1, dime::ypo::constant_<S>> &) -> indexing<D1, static_size<S>, 0>;
template<DimensionType D1, std::size_t S>
indexing(const dime::assigned_dim<D1, dime::ypo::constant_<S>> &) -> indexing<D1, static_size<S>, 0>;

template <typename... T, typename... U>
array_view(static_array<T...>&, const U & ...) -> array_view<static_array<T...>, typename index_converter<U>::type...>;
template <typename... T, typename... U>
array_view(dynamic_array<T...>&, const U & ...) -> array_view<dynamic_array<T...>, typename index_converter<U>::type...>;
template <typename... T, typename... U>
array_view(shared_array<T...>&, const U & ...) -> array_view<shared_array<T...>, typename index_converter<U>::type...>;
template <typename... T, typename... U>
array_view(array_view<T...>&, const U & ...) -> array_view<array_view<T...>, typename index_converter<U>::type...>;
};