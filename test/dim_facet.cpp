#include "gtest/gtest.h"
#include "dime/dynamic_array.hpp"
#include "dime/static_array.hpp"
#include "dime/extent.hpp"
#include "dime/types.hpp"

#include <iostream>
#include <array>
#include <vector>

namespace {

using namespace dime;

TEST(DimFacet, DynamicArrayWithArray) {
    dimension<int> x;

    std::array<int, 7> arr_7;
    dynamic_array da_x7C(x, arr_7);
    EXPECT_TRUE(
        (std::is_same_v<static_size<7>, decltype(da_x7C.dimSize(x))>));
	EXPECT_EQ(da_x7C.dimSize(x).size, 7);
}

TEST(DimFacet, DynamicArrayWithVector) {
    dimension<int> x;

    std::vector<int> vec_4 = std::vector<int>(4);

    dynamic_array da_x4(x, vec_4);

    EXPECT_TRUE(
        (std::is_same_v<dynamic_size, decltype(da_x4.dimSize(x))>));
	EXPECT_EQ(da_x4.dimSize(x).size, 4);
}

TEST(DimFacet, DynamicArrayWithNestedVector) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    std::vector<std::vector<int>> vec_5_3 = std::vector<std::vector<int>>(5, std::vector<int>(3));

    dynamic_array da_x5a3(z, vec_5_3);
    using dim_names_da_x5a3 = typename decltype(da_x5a3)::dims_names;
    EXPECT_EQ(std::tuple_size<dim_names_da_x5a3>::value, 2);
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<0, dim_names_da_x5a3>::type, char>));
    EXPECT_TRUE(
        (AnonymousDimension<typename dime::detail::get<1, typename decltype(da_x5a3)::dims_>::type>));
	EXPECT_EQ(da_x5a3.dimSize(z).size, 5);
    EXPECT_EQ(da_x5a3.dimSize<char>().size, 5);
    using second_dim = typename dime::detail::get<1, typename decltype(da_x5a3)::dims_>::type::dimension_name;
    EXPECT_EQ(da_x5a3.dimSize<second_dim>().size, 3);

    dynamic_array da_x5y3(x, y, vec_5_3);
    using dim_names_da_x5y3 = typename decltype(da_x5y3)::dims_names;
    EXPECT_EQ(std::tuple_size<dim_names_da_x5y3>::value, 2);
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<0, dim_names_da_x5y3>::type, int>));
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<1, dim_names_da_x5y3>::type, bool>));
	EXPECT_EQ(da_x5y3.dimSize(x).size, 5);
    EXPECT_EQ(da_x5y3.dimSize<int>().size, 5);
    EXPECT_EQ(da_x5y3.dimSize(y).size, 3);
    EXPECT_EQ(da_x5y3.dimSize<bool>().size, 3);
}

TEST(DimFacet, DynamicArrayExtent) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dimension<short> w;

    dynamic_array da_x4y3z5Cw1(x=4, y=3, z=5_c, w=1);
    using dim_names_da_x4y3z5Cw1 = typename decltype(da_x4y3z5Cw1)::dims_names;
    EXPECT_EQ(std::tuple_size<dim_names_da_x4y3z5Cw1>::value, 4);
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<0, dim_names_da_x4y3z5Cw1>::type, int>));
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<1, dim_names_da_x4y3z5Cw1>::type, bool>));
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<2, dim_names_da_x4y3z5Cw1>::type, char>));
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<3, dim_names_da_x4y3z5Cw1>::type, short>));
	EXPECT_EQ(da_x4y3z5Cw1.dimSize(x).size, 4);
    EXPECT_EQ(da_x4y3z5Cw1.dimSize<int>().size, 4);
    EXPECT_EQ(da_x4y3z5Cw1.dimSize(y).size, 3);
    EXPECT_EQ(da_x4y3z5Cw1.dimSize<bool>().size, 3);
    EXPECT_EQ(da_x4y3z5Cw1.dimSize(z).size, 5);
    EXPECT_EQ(da_x4y3z5Cw1.dimSize<char>().size, 5);
    EXPECT_EQ(da_x4y3z5Cw1.dimSize(w).size, 1);
    EXPECT_EQ(da_x4y3z5Cw1.dimSize<short>().size, 1);
}


TEST(DimFacet, DynamicArrayExtentWithArbitrary) {
    dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dimension<short> w;

    dynamic_array da_x4yAz5CwA(x=4, ~y, z=5_c, ~w);
    using dim_names_da_x4yAz5CwA = typename decltype(da_x4yAz5CwA)::dims_names;
    EXPECT_EQ(std::tuple_size<dim_names_da_x4yAz5CwA>::value, 4);
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<0, dim_names_da_x4yAz5CwA>::type, int>));
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<1, dim_names_da_x4yAz5CwA>::type, bool>));
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<2, dim_names_da_x4yAz5CwA>::type, char>));
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<3, dim_names_da_x4yAz5CwA>::type, short>));
	EXPECT_EQ(da_x4yAz5CwA.dimSize(x).size, 4);
    EXPECT_EQ(da_x4yAz5CwA.dimSize<int>().size, 4);
    EXPECT_EQ(da_x4yAz5CwA.dimSize(y).size, 1);
    EXPECT_EQ(da_x4yAz5CwA.dimSize<bool>().size, 1);
    EXPECT_EQ(da_x4yAz5CwA.dimSize(z).size, 5);
    EXPECT_EQ(da_x4yAz5CwA.dimSize<char>().size, 5);
    EXPECT_EQ(da_x4yAz5CwA.dimSize(w).size, 1);
    EXPECT_EQ(da_x4yAz5CwA.dimSize<short>().size, 1);
}

TEST(DimFacet, StaticArrayWithArray) {
	dimension<int> x;

    std::array<int,4> arr_4 = {1,2,3,4};

    static_array sa_x4C(x, arr_4);
    using dim_names_sa_x4CA = typename decltype(sa_x4C)::dims_names;
    EXPECT_EQ(std::tuple_size<dim_names_sa_x4CA>::value, 1);
    EXPECT_TRUE(
        (std::is_same_v<typename dime::detail::get<0, dim_names_sa_x4CA>::type, int>));
	EXPECT_EQ(sa_x4C.dimSize(x).size, 4);
    EXPECT_EQ(sa_x4C.dimSize<int>().size, 4);
}

TEST(DimFacet, StaticArrayWithNestedArray) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    std::array<std::array<std::array<int, 3>, 2>, 4> arr_4_2_3 = {};
    static_array sa_x4Cy2Cz3C(x, y, z, arr_4_2_3);
    EXPECT_EQ(sa_x4Cy2Cz3C.dimSize(x).size, 4);
    EXPECT_EQ(sa_x4Cy2Cz3C.dimSize(y).size, 2);
    EXPECT_EQ(sa_x4Cy2Cz3C.dimSize(z).size, 3);
}

TEST(DimFacet, StaticArrayExtent) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    static_array sa_x2C(x=2_c);
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_x2C.dimSize(x)), static_size<2>>));

    static_array sa_x2Cy3Cz5C(x=2_c, y=3_c, z=5_c);
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_x2Cy3Cz5C.dimSize(x)), static_size<2>>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_x2Cy3Cz5C.dimSize(y)), static_size<3>>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_x2Cy3Cz5C.dimSize(z)), static_size<5>>));
}

TEST(DimFacet, StaticArrayExtentWithArbitrary) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    static_array sa_xAy2C(~x, y=2_c);
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_xAy2C.dimSize(x)), arbitrary_size>));
    EXPECT_TRUE(
        (std::is_same<decltype(sa_xAy2C.dimSize(y)), static_size<2>>::value));

    static_array sa_x2CyAz5C(x=2_c, ~y, z=5_c);
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_x2CyAz5C.dimSize(x)), static_size<2>>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_x2CyAz5C.dimSize(y)), arbitrary_size>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_x2CyAz5C.dimSize(z)), static_size<5>>));
}

TEST(DimFacet, DataType) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;

    static_array sa_bool(~x, y=2_c, element_type<bool>);
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_bool.dimSize(x)), arbitrary_size>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_bool.dimSize(y)), static_size<2>>));

    static_array sa_string(x=1_c, y=2_c, z=3_c, element_type<char>);
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_string.dimSize(x)), static_size<1>>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_string.dimSize(y)), static_size<2>>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_string.dimSize(z)), static_size<3>>));

    std::array<std::string, 2> arr = {"te", "st"};
    static_array sa_arr(arr);
    using dim_names_sa_arr = typename decltype(sa_arr)::dims_names;
    EXPECT_EQ(std::tuple_size<dim_names_sa_arr>::value, 1);
    EXPECT_TRUE(
        (std::is_same_v<decltype(sa_arr.dimSize<typename dime::detail::get<0, dim_names_sa_arr>::type>()), static_size<2>>));

    dynamic_array da_bool(~x, y=2, element_type<bool>);
    EXPECT_TRUE(
        (std::is_same_v<decltype(da_bool.dimSize(x)), arbitrary_size>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(da_bool.dimSize(y)), dynamic_size>));
    EXPECT_EQ(da_bool.dimSize(y).size, 2);

    dynamic_array da_string(x=1_c, y=2, z=3_c, element_type<char>);
    EXPECT_TRUE(
        (std::is_same_v<decltype(da_string.dimSize(x)), static_size<1>>));
    EXPECT_TRUE(
        (std::is_same_v<decltype(da_string.dimSize(y)), dynamic_size>));
    EXPECT_EQ(da_string.dimSize(y).size, 2);
    EXPECT_TRUE(
        (std::is_same_v<decltype(da_string.dimSize(z)), static_size<3>>));

    std::vector<std::string> vec(4, "test");
    dynamic_array da_vec(vec);
    using dim_names_da_vec = typename decltype(da_vec)::dims_names;
    EXPECT_EQ(std::tuple_size<dim_names_da_vec>::value, 1);
    EXPECT_TRUE(
        (std::is_same_v<decltype(da_vec.dimSize<typename dime::detail::get<0, dim_names_da_vec>::type>()), dynamic_size>));
    EXPECT_EQ((da_vec.dimSize<typename dime::detail::get<0, dim_names_da_vec>::type>().size), 4);
}
}