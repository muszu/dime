#include "gtest/gtest.h"
#include "dime/extent.hpp"
#include "dime/dimensions.hpp"

namespace {

using namespace dime;

TEST(Extent, Creation) {
    dynamic_size ds(4);
    static_size<4> cs4;
    arbitrary_size as;
    dimension<struct x> x;

    extent<decltype(x), dynamic_size> e(x, ds);
    extent<decltype(x), static_size<4>> f(x, cs4);
    extent<decltype(x), arbitrary_size> g(x, as);
    auto a = extent(x, ds);
}

TEST(Extent, getSTLConatinerShape) {
    std::array<int, 1> a1{1};
    std::array<std::array<int, 1>, 2> a21{{{1}, {2}}};
    constexpr auto a1_shape_v1 = getSTLConatinerShape(a1);
    constexpr auto a1_shape_v2 = getSTLConatinerShape(a1);
    static_assert(! std::is_same_v<decltype(a1_shape_v1), decltype(a1_shape_v2)>);
    constexpr auto a21_shape_v1 = getSTLConatinerShape(a21);
    constexpr auto a21_shape_v2 = getSTLConatinerShape(a21);
    static_assert(! std::is_same_v<decltype(a21_shape_v1), decltype(a21_shape_v2)>);
}

}