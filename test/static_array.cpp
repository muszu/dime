#include "gtest/gtest.h"
#include "dime/static_array.hpp"
#include "dime/dynamic_array.hpp"
#include "dime/extent.hpp"
#include "dime/types.hpp"

#include <iostream>
#include <array>

using namespace dime;

namespace {

TEST(StaticArray, Constructor) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    dimension<struct u> u;
    dimension<struct v> v;
    constexpr static_array a(x=2_c);
    DeviceType cpu = device_type<Devices::CPU>;
    constexpr static_array b(cpu, z=3_c);
    constexpr static_array c(x=10_c, element_type<int>);
    constexpr static_array dd(x=4_c);
    constexpr static_array d(x=42_c, y=3_c, z=5_c);
    constexpr std::array<int,4> arr = {1,2,3,4};
    constexpr std::array<std::array<std::array<int, 1>, 2>, 4> arr3 = {};
    constexpr std::array<std::array<int, 2>, 4> arr2 = {};
    constexpr static_array e(arr);
    constexpr static_array f(x, arr, cpu);
    constexpr static_array g(x, y, arr3, cpu);
    constexpr static_array h(~x);
}

TEST(DynamicArray, CopyConstructor) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    std::array<int, 5> static_source = {{0,1,2,3}};

    static_array a(x, static_source);
    EXPECT_EQ(a.at(x=0), 0);
    EXPECT_EQ(a.at(x=1), 1);
    EXPECT_EQ(a.at(x=2), 2);
    EXPECT_EQ(a.at(x=3), 3);

    static_array b(a);
    EXPECT_EQ(b.at(x=0), 0);
    EXPECT_EQ(b.at(x=1), 1);
    EXPECT_EQ(b.at(x=2), 2);
    EXPECT_EQ(b.at(x=3), 3);

    shared_array c(x, static_source);
    EXPECT_EQ(c.at(x=0), 0);
    EXPECT_EQ(c.at(x=1), 1);
    EXPECT_EQ(c.at(x=2), 2);
    EXPECT_EQ(c.at(x=3), 3);

    static_array d(c);
    EXPECT_EQ(d.at(x=0), 0);
    EXPECT_EQ(d.at(x=1), 1);
    EXPECT_EQ(d.at(x=2), 2);
    EXPECT_EQ(d.at(x=3), 3);

    dynamic_array e(x, static_source);
    EXPECT_EQ(e.at(x=0), 0);
    EXPECT_EQ(e.at(x=1), 1);
    EXPECT_EQ(e.at(x=2), 2);
    EXPECT_EQ(e.at(x=3), 3);

    static_array f(e);
    EXPECT_EQ(f.at(x=0), 0);
    EXPECT_EQ(f.at(x=1), 1);
    EXPECT_EQ(f.at(x=2), 2);
    EXPECT_EQ(f.at(x=3), 3);
}

TEST(StaticArray, At) {
	dimension<int> x;
    dimension<bool> y;
    dimension<char> z;
    static_array a(x=2_c);
    a.at(x=0) = 2;
    a.at(x=1) = 3;
    EXPECT_EQ(a.at(x=0), 2);
    EXPECT_EQ(a.at(x=1), 3);
    EXPECT_EQ(a.at(x=1_c), 3);
    
    static_array b(x=1_c, y=2_c, z=4_c);
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 4; j++) {
            b.at(x=0, y=i, z=j) = i + j*2;
        }
    }
    bool match = true;
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 4; j++) {
            match = match && (b.at(x=0, y=i, z=j) == i + j*2);
        }
    }
    EXPECT_TRUE(match);
    
    static_array c(~z);
    c.at(z=100) = 5;
    c.at(z=24_c) = 42;
    EXPECT_EQ(c.at(z=0), 42);
}

}