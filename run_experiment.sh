#!/bin/bash

rm -rf build && mkdir build \
&& cmake -S . -B build -D CMAKE_BUILD_TYPE=Release \
    -D BUILD_TESTS=OFF \
    -D QCK_TEST=ON \
    -G Ninja \
&& cmake --build build

./build/test/experiment/experiment